'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require("gulp-rename");

gulp.task('sass', function () {
    return gulp.src('./themes/tuz/sources/scss/main.scss ')
        .pipe(sass().on('error', sass.logError))
        .pipe(rename(function (path) {
            path.basename = "tuz-main";
        }))
        .pipe(gulp.dest('./themes/tuz/public/css'));
});

gulp.task('sass:admin', function () {
    return gulp.src('./themes/admin/sources/scss/main.scss ')
        .pipe(sass().on('error', sass.logError))
        .pipe(rename(function (path) {
            path.basename = "admin-main";
        }))
        .pipe(gulp.dest('./themes/admin/public/css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./themes/tuz/sources/scss/**/*.scss', ['sass']);
});

var uglify = require('gulp-uglify');
var pump = require('pump');

gulp.task('compress', function (cb) {
    pump([
            gulp.src('public/js/cache/*.js'),
            uglify(),
            gulp.dest('public/dist/js')
        ],
        cb
    );
});




var async = require('async');
var iconfont = require('gulp-iconfont');
var consolidate = require('gulp-consolidate');

var iconfontCss = require('gulp-iconfont-css');

var fontName = 'dankoFont';

gulp.task('iconfont', function(){
    gulp.src(['assets/icons/*.svg'])
        .pipe(iconfontCss({
            cssClass: 'dfa',
            fontName: fontName,
            path: 'assets/css-templates/font.ejs',
            targetPath: 'danko-icons.css',
            fontPath: '/fonts/' + fontName + '/'
        }))
        .pipe(iconfontCss({
            cssClass: 'dfa',
            fontName: fontName,
            path: 'assets/html-templates/font.ejs',
            targetPath: 'danko-icons.html'
        }))
        .pipe(iconfont({
            fontName: fontName,
            normalize: true
        }))
        .pipe(gulp.dest('public/dist/css'));
});
