// var mongoose = require('mongoose');


var modules = require('modules');
var EventPage = modules.use('mongoose').getModel('EventPage');


module.exports.get = function (req, res) {

    EventPage.find({
        isHomeVisible: true,
        publish: true

    }, null, {
        sort: {
            eventDate: -1,
            created: -1
        }

    }, function (err, pages) {
        if (err) return next(err);

        res.render('home', {
            title: 'Home',
            isHomePage: true,
            pages: pages
        });
    });
};