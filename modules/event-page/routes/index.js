var moduleManager = require('moduleManager');
var isAdmin = moduleManager.use('admin').middlewares.isAdmin;
var switchTheme = moduleManager.use('adminUI').middlewares.switchTheme;
var redisStorage = moduleManager.use('redisStorage');
var log = moduleManager.use('log')(module);
var view = require('./view').get;
var validator = moduleManager.use('validator');

module.exports = function(app, cb) {
    app.get('/admin/event-page/list', isAdmin, switchTheme, require('./list').get);
    app.get('/admin/event-page/edit/:id', isAdmin, switchTheme, require('./edit').get);
    app.get('/admin/event-page/add', isAdmin, switchTheme, require('./edit').get);
    app.get('/event-page/:id', require('./view').get);

    cb();
};
