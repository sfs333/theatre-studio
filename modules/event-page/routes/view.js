var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var mongoose = moduleManager.use('mongoose');
var EventPage = mongoose.model('EventPage');

module.exports.get = function (req, res, next) {
    var id = mongoose.Types.ObjectId(req.params.id);

    EventPage.findOne({_id: id, publish: true}, function(err, page) {
        if (err) return next(err);
        if (!page) {
            var err = new Error('Page not found');
            err.status = 404;
            return next(err);
        }

        res.dependency('playerJs');
        res.set({ 'content-type': 'text/html; charset=utf-8' });
        res.render('event-page/view', {
            title: page.get('title'),
            page: page
        });
    });
};

