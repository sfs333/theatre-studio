var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var mongoose = moduleManager.use('mongoose');
var EventPage = mongoose.model('EventPage');

module.exports.get = function (req, res, next) {
    if (!req.params.id || !mongoose.isObjectId(req.params.id)) {

        res.render('event-page/edit', {
            title: 'Create Event Page',
            page: false
        });

    } else {
        EventPage.findById(req.params.id, function (err, page) {
            if (err) return next(err);
            if (!page) return next(new Error('No find page'));


            res.render('event-page/edit', {
                title: page ? page.get('title') : 'CREATE NEW EVENT PAGE',
                page: page
            });

        });
    }
};