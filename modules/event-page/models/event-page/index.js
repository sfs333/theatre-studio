var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
// var file = moduleManager.use('formFile').schema;
var mongoose = moduleManager.use('mongoose');
var Schema = mongoose.Schema;

var name = 'EventPage';
var opts = {};

var info = {
    authorID: require('./fields/authorID'),
    created: require('./fields/created'),
    eventDate: require('./fields/event-date'),
    title: require('./fields/title'),
    body: require('./fields/body'),
    picture: [],
    photos: [],
    publish: require('./fields/publish'),
    isHomeVisible: require('./fields/is-home-visabile'),
    type: require('./fields/type'),
    video: require('./fields/video'),
    path: require('./fields/path')
};

var schema = new Schema(info, opts);
mongoose.model(name, schema);