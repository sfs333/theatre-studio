module.exports = {
    pagination: {
        count: 50
    },
    fields: {
        title:{
            title: 'Title',
            type: 'string',
            params: {}
        },
        publish:{
            title: 'Publish',
            type: 'boolean'
        },
        isHomeVisible:{
            title: 'On home',
            type: 'boolean'
        },
        created:{
            title: 'Created',
            type: 'date',
            params: {
                format: 'DD.MM.YYYY h:mm:ss a'
            }
        },
        actions: {
            title: 'Actions',
            type: 'markup',
            params: {
                template: 'event-page/actions'
            }
        }
    }
};