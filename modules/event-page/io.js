var moduleManager = require('moduleManager');
var async = moduleManager.use('async');
var mongoose = moduleManager.use('mongoose');
var Page = mongoose.model('EventPage');
module.exports = function(io) {
    io.on('connection', function (socket) {
        socket.on('deletePage', function (info, callback) {
            if (!mongoose.isObjectId(info.id))
                return callback(new Error('id param not found'));

            async.waterfall([
                socket.request.loadUser,
                function(currentUser, callback) {
                    if (!currentUser.isAdmin())
                        return callback(new Error('Access denied!'));

                    Page.findById(info.id, callback);
                },
                function(page, callback) {
                    if (!page)
                        callback(new Error('Page not find'));
                    page.remove(callback);
                }
            ], function(err) {
                if (err)
                    return socket.request.error(err, callback);

                callback();
            });
        });
    });
}