var moduleManager = require('moduleManager');
var publicDepends = moduleManager.use('publicDepends');
var async = moduleManager.use('async');

require('./models');
require('./forms');
module.exports.run = function(app, cb) {
    require('./io')(app.get('io'));

    async.series([
        function(cb) {
            require('./routes')(app, cb);
        },
        function(cb) {
            require('./views')(cb);
        }
    ], cb);
};

publicDepends.addDependency({
    adminPageList: {
        scripts: ['js/admin/event-page/list'],
        dependencies: ['app']
    },
    playerJs: {
        scripts: ['js/event-page/page-player'],
        dependencies: ['app']
    }
});