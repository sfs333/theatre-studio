var modules = require('modules');
var async = modules.use('async');
var util = modules.use('util');
var validator = modules.use('validator');
var EventPage = modules.use('mongoose').model('EventPage');

module.exports = function(info) {
    info.on('validate', function (cb) {
        this.socket.request.loadUser(function(err, user) {
            if (err) return cb(err);

            if (!user.isAdmin)
                return cb(new Error('Access denied'));
            cb();
        });
    });
    info.on('validate', function (cb) {
        if (!validator.isMongoId(this.values.id))
            return cb(new Error('Not correct id'));
        cb();
    });
    info.on('submit', function (cb) {
        EventPage.remove({_id: this.values.id}, cb);

    });
    info.on('submit', function (cb) {
        this.response.actions.redirect = {url: 'admin/event-page/list'};
        cb();
    });
};