var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var mongooseForm = moduleManager.use('mongooseForm');

new mongooseForm.build('EventPage', require('./info'), function(err, mf) {
    if (err) return log.error(err);
    require('./events/index')(mf);
});