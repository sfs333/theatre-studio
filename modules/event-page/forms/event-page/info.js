module.exports = function () {
    this.name = 'admin-event-page-form';
    this.buttons = {
        submit: {title: 'save'},
        cancel: false
    }
    this.getFields = function () {
        return {
            title: {
                type: 'text',
                title: 'Title',
                placeholder: 'Enter title',
                required: true
            },
            type: {
                type: 'select',
                title: 'Type',
                items: {
                    'Концерт': 'Концерт',
                    'Выступление': 'Выступление',
                    'Спектакль': 'Спектакль',
                    'Репетиция': 'Репетиция',
                    'Этюд': 'Этюд',
                    '': 'Прочее'
                }
            },

            picture: {
                type: 'image',
                title: 'Picture',
                placeholder: 'Load picture',
                fileDir: 'eventPictures',
                allowTypes: ['png', 'jpeg'],
                maxSize: 4, //mb
                multiple: false
            },
            video: {
                type: 'text',
                title: 'Video link',
                placeholder: 'Just name file in the video directory',
                required: false
            },
            eventDate: {
                type: 'date',
                title: 'date',
                placeholder: 'Enter event Date'
            },
            body: {
                type: 'editor',
                title: 'Body',
                maxSize: 5
            },
            // photos: {
            //     type: 'file',
            //     title: 'Photos',
            //     placeholder: 'Load photos',
            //     fileDir: 'eventPhotos',
            //     allowTypes: ['png', 'jpeg'],
            //     maxSize: 8, //mb
            //     multiple: true
            // },
            // photos: {
            //     type: 'image',
            //     title: 'Images',
            //     placeholder: 'Load images',
            //     fileDir: 'eventPhotos',
            //     maxSize: 30,
            //     multiple: true,
            //     saveOriginalName: false,
            //     rewriteOriginal: 'watermark_origin',
            //     presets: {
            //         fieldPreview: {
            //             smartSize: {
            //                 width: 100,
            //                 height: 60
            //             }
            //         },
            //         size_194x140: {
            //             smartSize: {
            //                 width: 194,
            //                 height: 140
            //             }
            //         },
            //         'watermark_565x301': {
            //             resize: {
            //                 //width: 565,
            //                 height: 301
            //             },
            //             watermark: {
            //                 path: 'watermark_sm.png',
            //                 position: 'center'
            //             }
            //         },
            //         'watermark_origin': {
            //             maxSize: {
            //                 width: 1600,
            //                 height: 700
            //             },
            //             watermark: {
            //                 path: 'watermark_big.png',
            //                 position: 'center'
            //             }
            //         }
            //     }
            // },
            publish: {
                type: 'checkbox',
                title: 'Publish page'
            }
            ,
            isHomeVisible: {
                type: 'checkbox',
                title: 'Show on home page'
            }
        }
    }
}
;