(function(app) {
    var prepareRemove;
    app.on('formEvents', function($obj) {
        $obj.find('.view-item-tree-field a.remove').each(function() {
            var $link = $(this);
            $link.on('click', function(e) {
                e.preventDefault();
                var $self = $(this),
                    form,
                    $row = $self.parents('.tree-item');

                if ($obj.hasClass('app-form'))
                    form = $obj;
                else if ($obj.hasClass('form-popup'))
                    form = $obj.find('.app-form');
                else
                    form = $obj.parents('.app-form');
                prepareRemove($row.data('id'), $self.parents('.form-item').find('.form-control'), form, function() {
                    $row.fadeOut(150);
                });
                return false;
            });
        });
    });

    prepareRemove = function(id, $field, form, cb) {
        console.log(form);
        app.backend.emit('prepareRemoveItem', {
            formID: form.attr('id'),
            fieldName: $field.attr('name'),
            id: id
        }, cb);
    }
})(app);