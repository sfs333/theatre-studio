(function(app) {
    var async = app.module('async'),
        speed = 300,
        processProgress = {},
        rowTemplate = '<li class="tree-item no-save" data-parent="root" data-weight="0"></li>',
        progressTemplate = '<div class="progress-bar active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">0%</div>',
        progress = function($p, percent, text) {
            var t = false;

            if (((typeof percent === 'number') && ($p.attr('aria-valuenow') != percent))) {
                t = percent + '%';
                $p.attr('style', ('width:' + t));
                $p.attr('aria-valuenow', percent);
            }

            if (text)
                $p.html(text);
            else if (t)
                $p.html(t);
        }
    var makeProgress = function(classes) {
        var $p = $('<div class="progress"></div>').html(progressTemplate);

        $p.getBar = function() {
            return $(this).find('.progress-bar');
        }
        $p.animation = function(on) {
            this.getBar()[on ? 'addClass' : 'removeClass']('progress-bar-striped');
        }
        $p.set = function(percent, text) {
            progress(this.getBar(), percent, text);
        }
        $p.reset = function() {
            this.html(progressTemplate);
        }
        if (typeof classes === 'string')
            $p.addClass(classes);

        return $p;
    }
    var rowContent = function(fields) {
        var $content = $('<div class="row"></div>'),
            colCount = 12,
            $item,
            key,
            lg = (colCount / (Object.keys(fields).length - 1)).toFixed(0);

        if (lg > colCount) lg = colCount;
        else if (lg < 1) lg = 1;

        for (key in fields) {
            $item = $('<div class="view-item-tree-field">')
                .html(fields[key])
                .addClass('col-md-' + lg);
            $content.append($item);
        }
        return $content;
    }
    var fillRow = function($row, info) {
        $row.attr('data-id', info.id);
        $row.attr('data-name', ('item-' + info.id));
        $row.attr('data-weight', info.weight);

        delete info.id;
        delete info.parent;
        delete info.weight;
        $row.html(rowContent(info));
        app.emit('htmlPart', $row);
        return $row;
    }
    var uploadFile = function($field, $form, $progress, file, cb) {
        var $item = $field.parents('.form-item'),
            stream = ss.createStream(),
            size = 0,
            blobStream,
            $tree = $item.find('.tree-list'),
            multiple = ($field.attr('multiple') === 'multiple'),
            $row = $(rowTemplate);

        $progress.reset();
        $progress.animation(true);
        if (!multiple) {
            $tree.sortable("disable");
            $tree.addClass("disabled");
        }
        ss(app.backend.socket).emit('file', stream, {
            formID: $form.attr('id'),
            fieldName: $field.attr('name'),
            size: file.size,
            name: file.name,
            type: file.type
        }, function(response) {
            cb();
            if (!response.status)
                app.action('formErrorValidate', {message: response.message, field: $field.attr('name')}, $form);
            stream.destroy();
            delete blobStream;

            $tree[multiple ? 'append' : 'html'](fillRow($row, response.item));
        });
        blobStream = ss.createBlobReadStream(file);
        blobStream.on('data', function(chunk) {
            size += chunk.length;
            $progress.set(Number(((size / file.size) * 100).toFixed()));
        });

        blobStream.pipe(stream);
    }
    var task = function(file, $field, $form, $progress, tasks) {
        tasks.push(function(cb) {
            uploadFile($field, $form, $progress, file, cb);
        });
    }

    var saveProgress = function($p, formId, fieldName) {
        if (!processProgress[formId])
            processProgress[formId] = {};

        processProgress[formId][fieldName] = $p;
    }

    var getProgress = function(formId, fieldName) {
        if (!processProgress[formId])
            return false;

        if (!fieldName)
            return processProgress[formId] || {};

        return processProgress[formId][fieldName] || false;
    }

    var animationProcess = function(form, on) {
        var name,
            progresses = getProgress(form.attr('id'));
        if (!progresses)
            return false;

        for (name in progresses) {
            if (on)
                progresses[name].set(100, ' ');
            else
                progresses[name].reset();
            progresses[name].animation(on);
            progresses[name][on ? 'show' : 'hide']();
        }
    }

    app.on('formEvents', function(form) {
        if (!form.hasClass('app-form'))
            form = form.find('.app-form');
        form.find('input.file-field').each(function() {
            var $field = $(this),
                $wrap = $field.parent(),
                formID = form.attr('id'),
                fieldName = $field.attr('name');

            if ($wrap.find('.progress.process-progress').length === 0) {
                var $p = makeProgress('process-progress');
                $p.hide();
                $wrap.prepend($p);

                saveProgress($p, formID, fieldName);
            }
            $field.change(function(e) {
                var tasks = [];
                var i = 0;
                //var $p = getProgress(formID, fieldName);

                while (e.target.files[i]) {
                    task(e.target.files[i], $field, form, $p, tasks);
                    i++;
                }
                app.action('resetForm', form);
                app.action('lockForm', form);
                $field.attr('disabled', 'disabled');


                $p.width($field.width() + 28);
                $p.show();
                async.series(tasks, function(err) {
                    if (err) console.error(err);
                    $p.hide();
                    app.action('unlockForm', form);
                    $field.attr('disabled', false);
                });
            });
        });
    });

    app.on('submitForm', function(form) {
        animationProcess(form, true);

    });

    app.on('submittedForm', function(form) {
        animationProcess(form, false);
    });

    app.backend.on('formFileStatus', function(data) {
        if (!data.text) return false;
        var $p = getProgress(data.form, data.field);


        if ($p.length === 0)
            return false;

        $p.set(false, data.text);
        //$p.animation(true);
    });


})(app);