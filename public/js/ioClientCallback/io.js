(function(app) {
    app.backend.on('clientCallback', function(data) {
        app.action(data.action, data.params, function(err, response) {
            data.error = err;
            data.response = response;
            app.backend.emit('clientCallback', data, {second: 'argument'});
        });
    });
})(app);