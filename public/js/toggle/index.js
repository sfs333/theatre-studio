(function(app) {
    app.on('formEvents', function(form) {
        form.find('.toggle-checkbox  input[type="checkbox"]').each(function() {
            $(this).bootstrapToggle({
                width: 70,
                height: 27
            });
        });
    });
})(app);