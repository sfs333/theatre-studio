﻿(function(app) {
    app.backend = {
        socket: io(),
        emit: function(event) {
            console.error(event + ' don`t connect to socket!');
        },
        on: function(event, callback) {
            if (typeof event != 'string')
                return console.error('event param should be string type');
            if (typeof callback != 'function')
                return console.error('callback param should be function type');

            console.log('Add listener ' + event);
            app.backend.socket.on(event, callback);
        }
    }
    app.backend.on('connect', function() {
        console.log('success connect to socket');
        app.backend.emit = function(event, data, callback, context) {
            if (typeof event != 'string')
                return console.error('event param should be string type');

            console.log('Emit ' + event + ' event...');
            var context = context || false;
            var callback = (typeof callback == 'function') ? callback : false;

            app.backend.socket.emit(event, (data || {}), function() {
                if (typeof arguments[1] != 'undefined') {
                    err = arguments[0] || null;
                    var response = arguments[1];
                } else if ((typeof arguments[0] == 'object') && (arguments[0] !== null)) {
                    var response = arguments[0];
                    var err = response.err || null;
                } else {
                    var response = {};
                    var err = arguments[0] || null;
                }

                console.log('Emit ' + event + ' response');

                if (err) {
                    console.error('Error response from socket!');
                    if (err.message)
                        console.error(err.message);
                }
                //actions
                if (response && (typeof response.actions == 'object')) {
                    for (var action in response.actions)
                        app.action(action, response.actions[action], context)

                }
                if (callback)
                    callback(err, response);
            });
        }
    });

})(app);
