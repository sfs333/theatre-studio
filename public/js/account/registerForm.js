(function (app) {
    var effectTime = 200;
    var addPointer = function (form, info) {
        if (typeof info.field != 'string')
            return false;

        var wrap = form
            .find('[name="' + info.field + '"]')
            .parents('.form-item');
        var validator = wrap.find('.validator');
        if (validator.length == 0) {
            validator = $('<span class="validator"></span>').hide();
            wrap.append(validator);
            wrap.find('input[active-validate="true"], select[active-validate="true"]')
                .on('focus', function () {
                    validator.fadeOut(effectTime);
                })
                .on('blur', function () {
                    validator.fadeIn(effectTime);
                });
            validator.fadeIn(effectTime);
        }

    }

    app.on('userRegisterFormSuccessValidate', function (form, info) {
        addPointer(form, info);
    });
    app.on('userRegisterFormErrorValidate', function (form, info) {
        addPointer(form, info);
    });

    app.actions.submitRegisterForm = function (info, form) {
        var wrap = form.parents('.form-register-content'),
        startHeight = wrap.height();

        wrap.fadeOut(250, function () {
                wrap.css({'paddingLeft': '220px'})
                    .html(info.html)
                    .height(startHeight)
                    .fadeIn(250);
        });
    };

})(app);