(function(app) {
    var prepareRemove;
    app.on('formAddEvents', function(form) {
        form.find('.field-value-item a.remove').each(function() {
            var $link = $(this);
            $link.on('click', function(e) {
                e.preventDefault();
                var $self = $(this);
                var $row = $self.parents('.field-value-item');

                prepareRemove($row.data('id'), $self.parents('.form-item').find('.form-control'), form, function() {
                    $row.fadeOut(150);
                });
                return false;
            });

        });
    });

    prepareRemove = function(id, $field, form, cb) {
        app.backend.emit('prepareRemoveItem', {
            formID: form.attr('id'),
            fieldName: $field.attr('name'),
            id: id || null
        }, function() {
            console.log(arguments);
            cb();
        });
    }
})(app);