(function(app) {
    app.on('load', function() {
        $(".profile-tabs").tabs();
        $(".user-transactions-table, .user-paystats-table, .user-credit-purchases-table").tablesorter({
            theme          : 'dark',
            widgets        : ['zebra', 'columns'],
            usNumberFormat : false,
            sortReset      : true,
            sortRestart    : true
        });
    });
})(app);