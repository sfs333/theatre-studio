(function(app) {
    var popup = app.module('popup');
    app.on('popupEvents', function ($wrap) {
            $wrap = $wrap || null;
            $('.popup-form-marker', $wrap).on('click', function(e) {
                e.preventDefault();
                var $item = $(this);
                var $parent = $item.parent();

                var params = app.tools.getData($item);
                params.name = 'form';

                popup.open(params);
                return false;
            });
            popup.wrapper().on('hide.bs.modal', function() {
                $(this).find('form').each(function() {
                   app.backend.emit('dropFormItem', { id : $(this).attr('id') });
                });
            });
    });
})(app);