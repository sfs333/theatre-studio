(function(app) {
    var events = function ($wrap) {
        $wrap = $wrap || null;
        var popup = app.module('popup'),
            $img;
        var $wrapper = popup.createWrapper('img-prew-wrap');
        $('.popup-img-marker', $wrap).on('click', function(e) {
            e.preventDefault();
            var $self = $(this),
                path = $self.attr('data-path') || $self.attr('href');
            if (!path) return false;
            var css = {
                height: $(window).height() + 'px',
                width: ($(window).width() + 20) + 'px',
                backgroundImage: 'url("' + path + '")',
                backgroundRepeat: 'no-repeat'
            };

            var params = {};
            if ($self.attr('data-separate') == 'true')
                params.wrapper = $wrapper;

            //$wrapper.find('.modal-dialog').width($(document).width() / 1.7);
            //$wrapper.find('.modal-content').css({background: 'none'});

            //popup.width(($(window).width() + 20));
            popup.show($('<div />').css(css).addClass('img-in-popup'), params);
            $wrapper.addClass('popup-img');

            return false;
        });
    }

    app.on('popupEvents', events);
})(app);