(function (app) {

    var init = function ($wrapper) {
        var $player = $wrapper.find('video');
        var player = $player[0];

        if (!player) return;

        var playClass = 'video-playing';
        var $playBtn = $wrapper.find('.banner-play-btn');
        var $picture = $wrapper.find('.page-picture');

        $playBtn.on('click', function () {
            // if ($wrapper.hasClass(playClass))
            //     return player.pause();


            $wrapper.addClass(playClass);
            player.play();
        });

        // $player.on('pause', function () {
        //     $wrapper.removeClass(playClass);
        // });


    };


    var $wrapper = $('.video-banner');
    if ($wrapper.length > 0) init($wrapper);

})(app);

