(function (factory) {
    /* global define */
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else {
        // Browser globals: jQuery
        factory(window.jQuery);
    }
}(function ($) {
    // template
    var tmpl = $.summernote.renderer.getTemplate();
    var options = $.summernote.options;
    var eventHandler = $.summernote.eventHandler;
    var modules = eventHandler.modules;
    var handle = modules.handle;
    var toolbar = modules.toolbar;
    var editor = modules.editor;
    var buttons = [];

    var bar = function(action) {};
    modules.popover.update = function() {};

    var $inputWidth = $(tmpl.input('Width', {
        prefix: 'width',
        suffix: 'px',
        type: 'number',
        event: 'setWidth'
    }));
    var $inputHeight = $(tmpl.input('Height', {
        prefix: 'height',
        suffix: 'px',
        type: 'number',
        event: 'setHeight'
    }));
    var $saveRatio = $(tmpl.input('Save ratio', {
        type: 'checkbox',
        value: 1
    }));
    var $marginLeft = $(tmpl.input('Left', {
        prefix: 'left',
        suffix: 'px',
        type: 'number',
        event: 'setMarginLeft'
    }));
    var $marginRight = $(tmpl.input('Right', {
        prefix: 'right',
        suffix: 'px',
        type: 'number',
        event: 'setMarginRight'
    }));
    var $marginTop = $(tmpl.input('Top', {
        prefix: 'top',
        suffix: 'px',
        type: 'number',
        event: 'setMarginTop'
    }));
    var $marginBottom = $(tmpl.input('Top', {
        prefix: 'bottom',
        suffix: 'px',
        type: 'number',
        event: 'setMarginBottom'
    }));

    $.summernote.addPlugin({
        name: 'media',
        init : function(layoutInfo) {
            var $toolbar = layoutInfo.toolbar();
            bar = function(action) {
                action = action ? 'show' : 'hide';
                var finds = '';
                buttons.forEach(function(name) {
                    finds += 'button[data-name="' + name + '"], ';
                });
                $toolbar.find(finds.slice(0, -2))[action]();
            }
            bar(false);
            layoutInfo.editable().on('keyup mouseup', function(e) {
                var action = (e.target.nodeName === 'IMG');
                $toolbar.find('.btn-popover').fadeOut(150, function() {
                    setTimeout(function() {
                        bar(action);
                    }, 100);
                }).popover('hide');
            });
        },
        buttons: {
            imgResizeFull  : function (lang) {
                buttons.push('imgResizeFull');
                return tmpl.button('<span class="note-fontsize-10">100%</span>', {
                    event : 'resize',
                    value: '1',
                    title: lang.image.resizeFull
                });
            },
            imgResizeHalf  : function (lang) {
                buttons.push('imgResizeHalf');
                return tmpl.button('<span class="note-fontsize-10">50%</span>', {
                    event : 'resize',
                    value: '0.5',
                    title: lang.image.resizeHalf
                });
            },
            imgResizeQuarter  : function (lang) {
                buttons.push('imgResizeQuarter');
                return tmpl.button('<span class="note-fontsize-10">25%</span>', {
                    event : 'resize',
                    value: '0.25',
                    title: lang.image.resizeQuarter
                });
            },
            floatLeft: function(lang) {
                buttons.push('floatLeft');
                return tmpl.iconButton(options.iconPrefix + 'align-left', {
                    title: lang.image.floatLeft,
                    event: 'floatMe',
                    value: 'left'
                });
            },
            floatRight: function(lang) {
                buttons.push('floatRight');
                return tmpl.iconButton(options.iconPrefix + 'align-right', {
                    title: lang.image.floatRight,
                    event: 'floatMe',
                    value: 'right'
                });
            },
            imgRounded: function(lang) {
                buttons.push('imgRounded');
                return tmpl.iconButton(options.iconPrefix + 'square', {
                    title: lang.image.shapeRounded,
                    event: 'imageShape',
                    value: 'img-rounded'
                });
            },
            imgCircle: function(lang) {
                buttons.push('imgCircle');
                return tmpl.iconButton(options.iconPrefix + 'circle-o', {
                    title: lang.image.shapeCircle,
                    event: 'imageShape',
                    value: 'img-circle'
                });
            },
            imgThumbnail: function(lang) {
                buttons.push('imgThumbnail');
                return tmpl.iconButton(options.iconPrefix + 'picture-o', {
                    title: lang.image.shapeThumbnail,
                    event: 'imageShape',
                    value: 'img-thumbnail'
                });
            },
            imgRemove: function(lang) {
                buttons.push('imgRemove');
                return tmpl.iconButton(options.iconPrefix + 'trash-o', {
                    title: lang.image.remove,
                    event: 'removeMedia',
                    value: 'none'
                });
            },
            imgBlock: function(lang) {
                buttons.push('imgBlock');
                return tmpl.button('<span class="note-fontsize-10">Block</span>', {
                    event : 'imgBlock',
                    value: 'none',
                    title: 'Like block'
                });
            },
            imgResize: function(lang) {
                buttons.push('imgResize');
                var $popover = $(tmpl.iconButton(options.iconPrefix + 'arrows', {title: 'Setup Width Image', event: 'openPopover'}))
                    .addClass('btn-popover');

                var $content = $('<div><form></form></div>')
                    .find('form')
                    .append($saveRatio)
                    .append($inputWidth)
                    .append($inputHeight);

                $popover.popover({
                    placement : 'bottom',
                    html: true,
                    content: $content[0]
                }).on('show.bs.popover', function(e) {
                    var $selection = $(e.target).parents('.note-editor').find('.note-control-selection');
                    $target = $($selection.data('target'));
                    $inputWidth.find('input').val($target.width());
                    $inputHeight.find('input').val($target.height());
                });

                return $popover;
            },
            imgMargin: function(lang) {
                buttons.push('imgMargin');
                var $popover = $(tmpl.iconButton(options.iconPrefix + 'arrows-h', {title: 'Setup Margins Image', event: 'openPopover'}))
                    .addClass('btn-popover');

                var $content = $('<div><form></form></div>')
                    .find('form')
                    .append($marginLeft)
                    .append($marginRight)
                    .append($marginTop)
                    .append($marginBottom);

                $popover.popover({
                    placement : 'bottom',
                    html: true,
                    content: $content[0]
                }).on('show.bs.popover', function(e) {
                    var $selection = $(e.target).parents('.note-editor').find('.note-control-selection');
                    $target = $($selection.data('target'));
                    $marginLeft.find('input').val(Number($target.css('margin-left').slice(0, -2)));
                    $marginRight.find('input').val(Number($target.css('margin-right').slice(0, -2)));
                    $marginTop.find('input').val(Number($target.css('margin-top').slice(0, -2)));
                    $marginBottom.find('input').val(Number($target.css('margin-bottom').slice(0, -2)));
                });

                return $popover;
            }
        },

        events: {
            setMarginBottom: function($editable, value, $target) {
                $target.css({marginBottom: (value + 'px')});
            },
            setMarginTop: function($editable, value, $target) {
                $target.css({marginTop: (value + 'px')});
            },
            setMarginRight: function($editable, value, $target) {
                $target.css({marginRight: (value + 'px')});
            },
            setMarginLeft: function($editable, value, $target) {
                $target.css({marginLeft: (value + 'px')});
            },
            setWidth: function($editable, value, $target) {
                if ($saveRatio.find('input').is(':checked')) {
                    var ratio = $target.width() / $target.height();
                    var height = (value / ratio).toFixed();
                    $target.height(height);
                    $inputHeight.find('input').val(height);
                }
                $target.width(value);
            },
            setHeight: function($editable, value, $target) {
                if ($saveRatio.find('input').is(':checked')) {
                    var ratio = $target.width() / $target.height();
                    var width = (value * ratio).toFixed();
                    $target.width(width);
                    $inputWidth.find('input').val(width);
                }
                $target.height(value);
            },
            resize: function (event, editor, layoutInfo, value) {
                var $selection = layoutInfo.handle().find('.note-control-selection');
                $editable = layoutInfo.editable();
                $target = $($selection.data('target'));
                var widthDif = $target.width() - ($editable.width() * (value));

                editor.beforeCommand($editable);
                $target.css({
                    width: (widthDif < 3) && (widthDif > -3) ? '' : ((value * 100) + '%'),
                    height: ''
                });
                editor.afterCommand($editable);

                setTimeout(function() {
                    //handle.update($handle, {image: $target[0]}, false);
                    toolbar.update(layoutInfo.toolbar(), {image: $target[0]});
                }, 1);
            },
            imgBlock: function (event, editor, layoutInfo, value) {
                var $selection = layoutInfo.handle().find('.note-control-selection');
                $editable = layoutInfo.editable();
                $target = $($selection.data('target'));

                editor.beforeCommand($editable);
                $target.css({ display: ($target.css('display') == 'block') ? '' : 'block' });
                editor.afterCommand($editable);
            },
            floatMe: function (event, editor, layoutInfo, value) {
                var $selection = layoutInfo.handle().find('.note-control-selection');
                $editable = layoutInfo.editable();
                $target = $($selection.data('target'));
                var current = $target.css('float');

                editor.beforeCommand($editable);
                $target.css('float', (current === value ? 'none' : value));
                editor.afterCommand($editable);

                setTimeout(function() {
                    //handle.update($handle, {image: $target[0]}, false);
                    toolbar.update(layoutInfo.toolbar(), {image: $target[0]});
                }, 1);
            },
            imageShape: function (event, editor, layoutInfo, value) {
                var $selection = layoutInfo.handle().find('.note-control-selection');
                $editable = layoutInfo.editable();
                $target = $($selection.data('target'));
                var isActive = $target.hasClass(value);
                $target.removeClass('img-rounded img-circle img-thumbnail');

                editor.beforeCommand($editable);

                if (value && !isActive)
                    $target.addClass(value);

                editor.afterCommand($editable);

                setTimeout(function() {
                    //handle.update($handle, {image: $target[0]}, false);
                    toolbar.update(layoutInfo.toolbar(), {image: $target[0]});
                }, 1);
            }
        }
    });
}));
