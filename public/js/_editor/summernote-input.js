(function (factory) {
    /* global define */
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else {
        // Browser globals: jQuery
        factory(window.jQuery);
    }
}(function ($) {
    // template
    var renderer = $.summernote.renderer;
    var eventHandler = $.summernote.eventHandler;
    var input = function(title, opt) {
        if (typeof opt !== 'object') opt = {};
        var isCheckbox = opt.type === 'checkbox';
        var html = '<div class="input-group">';
        if (opt.prefix) html += ('<span class="input-group-addon">' + opt.prefix + '</span>');
        html += '<input class="form-control" aria-label="' + (title || '') + '" type="' + (opt.type || 'text') + '" ' +
        (opt.event ? ( 'data-input-event="' + opt.event + '"' ) : '' ) +
        ((isCheckbox && opt.value === 1) ? 'checked="checked"' : '') + ' />';
        if (isCheckbox)
            html += '<label>' + title + '</label>'
        if (opt.suffix) html += ('<span class="input-group-addon">' + opt.suffix + '</span>');
        html += '</div>';

        return html;
    }

    var templates = renderer.getTemplate();
    renderer.getTemplate = function() {
        return $.extend(templates, {input: input});
    }

    $.summernote.addPlugin({
        name: 'input',
        init : function(layoutInfo) {},
        buttons: {},
        events: {
            openPopover: function(event, editor, layoutInfo) {
                var $selection = layoutInfo.handle().find('.note-control-selection');
                $editable = layoutInfo.editable();
                $target = $($selection.data('target'));

                $(event.target).next('.popover').find('input').each(function() {
                    var $self = $(this);
                    var eventName;
                    if (!(eventName = $self.attr('data-input-event')))
                        return;
                    $self.trigger('showPopover', [$target]);
                    $self.on('change', function(event) {
                        if ($.isFunction($.summernote.pluginEvents[eventName])) {
                            $editable.focus();
                            $.summernote.pluginEvents[eventName]($editable, $self.val(), $target);
                        }
                    });
                });
            }
        }
    });
}));
