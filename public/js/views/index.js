(function(app) {
    var parts = app.module('parts');
    var async = app.module('async');
    var effectSpeed = 100;
    var view = app.module('view', {});

    var updateView = function(view, opt, cb) {
        var displayID = view.data('display');
        var viewName = view.data('view');
        var $content = view.find('.view-content');
        opt.view = viewName;
        opt.display = displayID;
        opt.handler = 'table';
        opt.theme = view.attr('data-theme') || null;
        async.waterfall([
            function(cb) {
                $content.animate({opacity: 0}, effectSpeed, cb);
            },
            function(cb) {
                parts.render('loadView', opt, cb);
            },
            function(data, cb) {
                $content.html(data.$el);
                viewContentEvents(view);
                $content.animate({opacity: 1}, effectSpeed, cb);
            }
        ], cb);
    }
    var pagination = function(paginationBox, view) {
        var $links = paginationBox.find('li a');

        $links.on('click', function() {
            var $self = $(this);
            var pageNumber = $self.data('page');
            if ($self.hasClass('active'))
                return false;
            updateView(view, {page: pageNumber}, function(err) {
                if (err) return app.error(err);

                $links.removeClass('active');
                $self.addClass('active');
                app.emit(('update-view-' + view.data('view') + '-' + view.data('display')), view);
            });
        })
    }

    var tableSort = function(table, view) {
        table.find('thead th a').on('click', function(e) {
            e.preventDefault();
            var $self = $(this);
            var name = $self.data('name');
            var direction = $self.hasClass('sort-up');
            if (name) {
                updateView(view, {sort: {field: name, direction: direction}}, function(err) {
                    if (err) return app.error(err);
                    var $link = view.find('thead th a.sort-link.' + name);
                    $link.addClass(direction ? 'sort-down' : 'sort-up');
                    app.emit(('update-view-' + view.data('view') + '-' + view.data('display')), view);
                });
            }
            return false;
        });
    }

    var tableAttachEvents = function(table, view) {
        tableSort(table, view);
    }

    var viewContentEvents = function(view) {
        var $table = view.find('.view-table table');
        if ($table.length > 0)
            tableAttachEvents($table, view);
    }

    var events = function(view) {
        var $pagination = view.find('.pagination');
        if ($pagination.length > 0)
            pagination($pagination, view);
        viewContentEvents(view);
    }

    app.on('viewEvents', function($context) {
       if ($context.hasClass('view-box'))
           events($context);
        $('.view-box', $context).each(function() {
            events($(this));
        });
    });
    app.on('load', function () {
        app.emit('viewEvents', $('body'));
    });
    app.on('htmlPart', function ($el) {
        app.emit('viewEvents', $el);
    });
})(app);