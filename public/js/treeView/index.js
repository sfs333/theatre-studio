(function(app) {
    var treeView = app.module('treeView', {});
    var wrapperTemplate = '.tree-wrapper';
    var rootName = 'root';
    var maxDifference = 10;
    var delay = 200;
    var tolerance = 20;
    var containerSelector = 'ol, ul';
    var itemSelector = '.tree-item';
    var timeEffects = 300;
    var showAttr = function($item, attr) {
        $item.find('div.' + attr + '-monitor').first().find('span').text($item.attr('data-' + attr));
    }
    var setWeight = function($item) {
        var itemId = $item.attr('data-id');
        var $el;
        var id;
        var lastWeight = 0;
        var curWeight = 0;
        $item.siblings(itemSelector).andSelf().each(function() {
            $el = $(this);
            curWeight = Number($el.attr('data-weight'));
            id = $el.attr('data-id');
            if ((curWeight <= lastWeight) || ((id === itemId) && ((curWeight - lastWeight) > 1)) || ((curWeight - lastWeight) > maxDifference)) {
                curWeight = (lastWeight + 1);
                $el.attr('data-weight', curWeight);

                //debug
                if (id !== itemId)
                    $el.addClass('changed');
                //showAttr($el, 'weight');
            }
            lastWeight = curWeight;
        });
    };
    var setParent = function($item) {
        var parent = $item.parents(itemSelector).first();
        parent = (!parent || (parent.length === 0)) ? rootName : parent.attr('data-id');
        $item.attr('data-parent', parent);
        //debug
        //showAttr($item, 'parent');
    };
    var getChangedData = function($saveElements) {
        var changed = [];
        $saveElements.each(function() {
            changed.push(app.tools.getData($(this)));
        });
        return changed;
    }
    var send = function($treeWrapper, cb) {
        var $saveElements = $treeWrapper.find(itemSelector + '.changed');

        var data = {
            items: getChangedData($saveElements),
            name: $treeWrapper.data('name'),
            context: app.tools.getData($treeWrapper.parents('.view-box')) || {}
        };
        app.backend.emit('treeViewProcess', data, function(err) {
            if (err)
                return console.error(err);

            $saveElements.removeClass('changed');
            $saveElements.addClass('success');
            setTimeout(function() {
                $saveElements.removeClass('success');
            }, timeEffects);

            if (typeof cb === 'function')
                cb();
        });
    }
    var submitBtn = function($treeWrapper, $submitBtn) {
        $submitBtn.on('click', function(e) {
            e.preventDefault();
            $(this).addClass('disabled');
            send($treeWrapper);

            return false;
        });
    }
    app.on('treeViewEvents', function($treeWrapper) {
        var autosubmit = $treeWrapper.hasClass('autosubmit'),
            oldContainer,
            $submitBtn;

        if (!autosubmit) {
            $submitBtn = $treeWrapper.find('.tree-submit');
            submitBtn($treeWrapper, $submitBtn);
        }

        $treeWrapper.find(".tree-list").sortable({
            group: 'nested',
            delay: delay,
            tolerance: tolerance,
            containerSelector: containerSelector,
            itemSelector: itemSelector,
            //containerPath: '.row, ol .row',
            afterMove: function (placeholder, container) {
                if (oldContainer != container){
                    if(oldContainer)
                        oldContainer.el.removeClass("active");
                    container.el.addClass("active");

                    oldContainer = container;
                }
            },
            onDrop: function ($item, container, _super) {
                container.el.removeClass("active");
                _super($item);

                $item.addClass('changed');

                setWeight($item);
                setParent($item);

                if (autosubmit) {
                    send($treeWrapper);
                } else if ($submitBtn.hasClass('disabled')) {
                    $submitBtn.removeClass('disabled');
                }

            }
        });
    });

    app.on('htmlPart', function($el) {
        app.emit('treeViewEvents', $(wrapperTemplate, $el));
    });
})(app);