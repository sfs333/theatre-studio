(function(app) {
    var popup = app.module('popup');
    var messages = {
        remove: 'Are you sure?'
    };
    app.on('load', function () {
        var $table = $('.view-page-adminList');
        var attachEvents = function(wrap) {
            wrap.find('.link-page-delete').on('click', function (e) {
                var $self = $(this);
                popup.confirm(messages.remove, function() {
                    var id = $self.data('id');
                    if (!id) return;

                    app.backend.emit('deletePage', {id: id}, function(err, response) {
                        if (err) return console.error(err);

                        app.action('redirect', {url: ''});
                    });
                }, function() {}, {width: 300});
                e.preventDefault();
                return false;
            });
        };

        attachEvents($table);
        app.on('update-view-page-adminList', function(wrap) {
            attachEvents(wrap);
        });
    });
})(app);

