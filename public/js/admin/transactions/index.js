(function(app) {
    app.on('load', function () {
        var $table = $('.admin-transactions-table');
        $table.tablesorter({
            theme          : 'dark',
            widgets        : ['zebra', 'columns'],
            usNumberFormat : false,
            sortReset      : true,
            sortRestart    : true
        });
    });
})(app);