(function(app) {
    app.on('load', function() {
        var $helpWrap = $('.admin-nav .form-helps');
        if ($helpWrap.length === 0) return false;

        app.on('formEvents', function ($wrap) {
            var $forms = $wrap.hasClass('app-form') ? $wrap : $wrap.find('.app-form');

            //if ($forms.length === 0)
            //    return false;

            var handler = function($form) {
                var formID = $form.attr('id');
                if ($helpWrap.attr('data-form') == formID)
                    return false;

                console.log("SET");
                $helpWrap.html("");
                $helpWrap.attr('data-form', formID);
                $form.find('.form-actions .buttons .btn').each(function() {
                    var $self = $(this),
                        $duplicate = $self.clone();
                    $duplicate.on('click', function() {
                        $self.trigger('click');
                    });
                    $helpWrap.append($duplicate);
                });
            }
            $forms.find('input').on('focus', function() {
                handler($(this).parents('form.app-form'));
             });

            $forms.find('.previews').on('click', function() {
                handler($(this).parents('form.app-form'));
             });

            //$('body').on('click', function(e) {
            //    if ($(e.target).parents('form.app-form').length === 0)
            //        $helpWrap.html("");
            //});
        });
    });
})(app);