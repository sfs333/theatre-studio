var path = require("path");
var winston = require('winston');
var fs = require("fs");
var util = require("util");
var async = require("async");

var corePath = path.join( __dirname, '/..');
var coreModules = path.join(corePath, 'modules');
var customModules = path.join(corePath, '../modules');

var storage = {};

var log = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            colorize: true,
            label: 'Module System'
        })
    ]
});

module.exports.register = function(name, filePath, absolute) {
    var absolute = absolute || false;
    storage[name] = path.join((absolute ? '' : corePath), filePath);
}


module.exports.use = function(name, path) {
    if (typeof storage[name] == 'string')
        return require(storage[name] + (path || ''));

    return require(name);
}

module.exports.show = function() {
    console.log(storage);
}

module.exports.loadDefault = function(app) {
    var config = module.exports.use('config');
    var async = require("async");
    var runs = [];

    config.get('autoRunModules').forEach(function(name) {
        var moduleIndex = module.exports.use(name);
        if (typeof moduleIndex.run == 'function')
            runs.push({name: name, run: moduleIndex.run});

//        if (typeof moduleIndex.publicDependency == 'function')
//            publicDependencies.addDependency(moduleIndex.publicDependency());
    });

    async.eachSeries(runs, function(item, callback) {
        log.info('Module "' + item.name + '" loading...');
        item.run(app, callback);
    },
    function() {
        log.info('All modules is loaded');
    });

}

module.exports.scanModulePath = function(scanPath, cb) {
    if (!fs.existsSync(scanPath))
        return cb(new Error(util.format('Path - "%s" with modules not exist'), scanPath));


    var files = fs.readdirSync(scanPath);
    files.forEach(function (name) {
        var mPath = path.join(scanPath, (name));
        var indexPath = path.join(mPath, 'index.js');
        if (fs.existsSync(indexPath))
            module.exports.register(name, mPath, true);
        else
            log.warn(util.format('Module %s not found', name));
    });

    cb();
}

module.exports.run = function() {
    async.series([
        function(cb) {
            module.exports.scanModulePath(coreModules, cb);
        },
        function(cb) {
            module.exports.scanModulePath(customModules, cb);
        }
    ], function(err) {
        if (err) return log.error(err);
    });

    return module.exports;
}