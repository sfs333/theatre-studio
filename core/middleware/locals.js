var modules = require('modules');
var config = modules.use('config');
var async = require('async');
var variables = modules.use('variables');
var locals = modules.use('locals');
var _ = modules.use('underscore');



module.exports = function(req, res, next) {

    if (!res.locals)
        res.locals = {};

    res.locals.metatags = {};

    _.extend(res.locals, locals);

    res.jsValues = res.locals.jsValues;

    return next();

    // res.locals.jsValues = res.jsValues = {};
    // //res.locals.siteName = config.get('siteName');
    // res.locals.supportEmail = config.get('supportEmail');
    // res.locals.adminEmail = config.get('adminEmail');
    // res.locals.siteDomain = config.get('siteDomain');
    // //res.locals.logoFileName = config.get('logoFileName');
    //
    // console.log();
    //
    // async.parallel([
    //     function(cb) {
    //         variables.get('siteName', function(err, value) {
    //             if (err) return cb(err);
    //             res.locals.siteName = value;
    //             cb();
    //         });
    //     },
    //     function(cb) {
    //         variables.get('siteSlogan', function(err, value) {
    //             if (err) return cb(err);
    //             res.locals.siteSlogan = value;
    //             cb();
    //         });
    //     },
    //     function(cb) {
    //         variables.get('logoFileName', function(err, value) {
    //             if (err) return cb(err);
    //             res.locals.logoFileName = value;
    //             cb();
    //         });
    //     }
    // ], next);
};