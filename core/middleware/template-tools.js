var modules = require('modules');
var config = modules.use('config');
var async = require('async');
var variables = modules.use('variables');
var _ = modules.use('lodash');

module.exports = function(req, res, next) {

    _.extend(res.locals, {
        subString: function (string, length, pointers) {

            var response = string.substring(0, (length || 100));

            if (response.length < string.length && pointers !== false)
                response += '...';

            return response;
        },
        clearHtml: function (string) {

            return string.replace(/(\<(\/?[^>]+)>)/g, '');
        }
    });


    next();
};