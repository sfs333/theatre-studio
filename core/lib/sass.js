var moduleManager = require('moduleManager');
//var sass = require('node-sass');
var path = require('path');
var fs = require('fs');
var async = require('async');
var bytes = require('bytes');
var clc = require('cli-color');
var log = moduleManager.use('log')(module);
var config = moduleManager.use('config');

var compileCSS = function(info, callback) {

    var stats = {};

    async.waterfall([
        function(callback) {

            sass.render({
                file: info.sourcesFile,
                outputStyle: info.outputStyle,
                stats: stats,
                success: function(result) {
                    callback(null, result.css);
                },
                error: function(error) {
                    callback(new Error(error));
                }
            });

        }, function(css, callback) {
            fs.writeFile(info.outputCssFile, css, {flag: 'w+'}, function(err) {
                callback(err, {
                        size: bytes(css.length),
                        ms: (stats.end - stats.start) + 'ms'
                });
            });
        }

    ], callback);
}

var compile = function(theme) {
    var root = path.join(__dirname, '../..');
    var themePath = path.join(root, 'themes/' + theme);
    var outputCssFile =  path.join(root, 'public/css/' + theme + '-main.css');
    var sourcesFile = path.join(themePath, 'sources/scss/main.scss');

    fs.exists(sourcesFile, function (exists) {
        if (exists) {
            compileCSS({
                    sourcesFile: sourcesFile,
                    outputCssFile: outputCssFile,
                    outputStyle: 'expanded' /* 'compressed' */
                },
                function(err, results) {
                    if (err) return log.error(err.message);

                    log.info('SASS success compiled! (' + clc.cyan(results.size) + ', from ' + clc.cyan(results.ms) + ') Path: "' + outputCssFile + '"');

                });
        }
    });
}
module.exports = function(app) {
    //compile(config.get('theme'));
    //compile(config.get('adminTheme'));
}