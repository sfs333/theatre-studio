var remoteAddress = function(req) {
    if (!req.connection)
        return null;

    if (req.headers['x-real-ip'])
        return req.headers['x-real-ip'];

    if (req.connection.remoteAddress)
        return req.connection.remoteAddress.replace(/[\: f]/g, '');

    if (req.socket)
        return req.socket.socket ? req.socket.socket.remoteAddress : req.socket.remoteAddress;

    return null;
};

var getReqArg = function(n, req) {
    if (typeof n !== 'number')
        return req.urlArgs;

    return req.urlArgs[n - 1];
};

module.exports = function(req, res, next) {
    req.getIP = function() {
        return remoteAddress(req);
    };
    req.arg = function(n) {
        return getReqArg(n, req);
    };

    // if (!req.headers.accept || !Boolean(req.headers.accept.indexOf('text/html') + 1)) {
    //     var err = new Error('File not found!');
    //     err.status = 404;
    //     err.template = 'fileNotFound';
    //     return next(err);
    // }
    
    req.urlArgs = req.url.substr(1).split('/');
    next();
};