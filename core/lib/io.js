var moduleManager = require('moduleManager')
var log = moduleManager.use('log')(module);
var config = moduleManager.use('config');
var async = require('async');
var cookie = require('cookie');
var cookieParser = require('cookie-parser');
var sessionStore = require('lib/sessionStore');
var url = require('url');

function loadSession(sid, callback) {
    sessionStore.load(sid, function(err, session) {
        if (err) return callback(err);
        callback(err, ((arguments.length === 0) ? null : session));
    });
}

function saveSession(sid, session, callback) {

    sessionStore.set(sid, session, function(err) {
        if (arguments.length === 0) {
            return callback(null, null);
        } else {
            return callback(null, session);
        }
    });
}

function getSidFromIO(handshake) {
    handshake.cookies = cookie.parse(handshake.headers.cookie || '');

    var sidCookie = handshake.cookies[config.get('session:key')];
    if (typeof sidCookie !== 'string') {
        log.warn('Cookie not found!');
        return false;
    }
    
    return cookieParser.signedCookies({sid: sidCookie}, config.get('session:secret')).sid;
}

module.exports = function(server) {
    var io = require('socket.io')(server);
//    io.set('origins', '192.168.1.*:3000');
//    io.set('logger', log);
    //io.set('transports', [ 'websocket', 'polling' ]);
    io.sockets.setMaxListeners(0);
    
    io.set('authorization', function (handshake, callback) {
        async.waterfall([
            function(callback) {

                handshake.sessionManager = {
                    sid: getSidFromIO(handshake),
                    get: function(callback) {
                        if (!this.sid) return {};
                        loadSession(this.sid, function(err, session) {
                            if (err) return callback(err);
                            callback(err, session);
                        });
                    },
                    set: function(session, callback) {
                        if (!this.sid) return;
                        saveSession(this.sid, session, function(err) {
                            if (err) {
                                callback(err);
                                return;
                            }

                            callback(err, session);
                        });
                    }
                };

                callback(null);
            }
        ], function(err) {
            if (err) {
                callback(err);
                return;
            }

            callback(null, true);
        });
    });

    io.use(function(socket, next) {
        socket.request.getUrl = function(callback) {
            var referer = socket.handshake.headers.referer;
            if ((typeof referer === 'string') && referer.length > 0) {

                var parsed = url.parse(referer);
                parsed.arguments = parsed.path.substr().split('/');
                return parsed;
            }

            log.error('Error parse url in socket');

            return {};
        };

        socket.request.error = function(err, callback) {
            console.log(err);
            if (typeof err.message === 'string')
                return callback({message: err.message}, {});

            return callback({message: 'System error.'}, {});
        };

        next(null);
    });
    return io;
};