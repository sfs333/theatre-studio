var modules = require('modules');
var config = modules.use('config');
var async = require('async');
var variables = modules.use('variables');
var log = modules.use('log')(module);

var data = {
    jsValues: {},
    supportEmail: config.get('supportEmail'),
    adminEmail: config.get('adminEmail'),
    siteDomain: config.get('siteDomain')
};

async.parallel([
    function(cb) {
        variables.get('siteName', function(err, value) {
            if (err) return cb(err);
            data.siteName = value;
            cb();
        });
    },
    function(cb) {
        variables.get('siteSlogan', function(err, value) {
            if (err) return cb(err);
            data.siteSlogan = value;
            cb();
        });
    },
    function(cb) {
        variables.get('logoFileName', function(err, value) {
            if (err) return cb(err);
            data.logoFileName = value;
            cb();
        });
    }
], function (err) {
    if (err) return log.error(err);
});

module.exports = data;