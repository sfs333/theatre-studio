var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var async = require('async');
var util = require('util');

var serialEE = function() {
    this.run();
}

serialEE.prototype.run = function() {
    this.listeners = { all:[]};
}

serialEE.prototype.use = function(event, listener) {
    if (!util.isArray(this.listeners[event]))
        this.listeners[event] = [];

    if (typeof listener == 'function')
        this.listeners[event].push(listener);
    else
        log.warn('Listener should be is function');
}

serialEE.prototype.emit = function(event, env, cb) {
    var lastArguments = [];
    var environment = this;

    var listeners = this.listeners['all'].concat(this.getListeners(event));
    if ((arguments.length === 3) && (typeof env === 'object'))
        environment = env;
    if (typeof arguments[1] === 'function')
        cb = arguments[1];
    async.eachSeries(listeners, function(item, cb) {
        lastArguments.push(function(err) {
            lastArguments = Array.prototype.slice.call(arguments).splice(1);
            cb(err);
        });
        item.apply(environment, lastArguments);

    }, function(err) {
        lastArguments.unshift(err || null);
        if (typeof cb === 'function')
            cb.apply(environment, lastArguments);
    });
}

serialEE.prototype.getListeners = function (event) {
    if (util.isArray(this.listeners[event]))
       return this.listeners[event];

    return [];
}

module.exports = serialEE;