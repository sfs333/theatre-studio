var transliterate = require('transliteratsia');
transliterate.region = 'ru';

module.exports = function (text) {

    return transliterate(text.match(/[а-яА-ЯёЁA-Za-z ]/gi).join('').replace(/[ ]{1,}/g, '-')).toLowerCase();
};