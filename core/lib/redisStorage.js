var _redis = require("redis");
var moduleManager = require('moduleManager');
var redis = _redis.createClient();
var config = moduleManager.use('config');
var prefix = (config.get('redisStorage:prefix') || '') + '_';

var stringGet = redis.get;
var stringSet = redis.set;

redis.set = function(key, value, callback) {
    if (typeof key != 'string')
        return callback(new Error('Don`t correct key for save in storage'));

    if ((!value && (typeof value != 'boolean')) || (typeof value == 'function'))
        return callback(new Error('Don`t correct value for save in storage'));

   stringSet.apply(redis, [(prefix + key), JSON.stringify({value: value}), callback]);
}

redis.get = function(key, callback) {
    if (typeof key != 'string')
        return callback(new Error('Don`t correct key for save in storage'));

    stringGet.apply(redis, [(prefix + key), function(err, valueString) {
        if (err) return callback(err);

        var value = false;
        if (valueString !== null)
            value = JSON.parse(valueString).value;
        callback(null, (value || null));
    }]);
}

module.exports = redis;