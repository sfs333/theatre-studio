var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
//Custom
var moduleManager = require('moduleManager').run();
require('./lib');

moduleManager.register('localsMiddleware', '/middleware/locals.js');
moduleManager.register('momentMiddleware', '/middleware/moment.js');
moduleManager.register('templateTools', '/middleware/template-tools.js');

var config = moduleManager.use('config');
var theme = moduleManager.use('theme');
var themePath = theme.getTheme();
var log = moduleManager.use('log')(module);
var session = require('express-session');

var sessionOptions = config.get('session');
sessionOptions.store = require('lib/sessionStore');


var app = express();

//system setup
app.set('env', config.get('env'));

// view engine setup
app.engine('ejs', require('ejs-locals'));
app.set('view engine', 'ejs');
app.set('views', themePath + '/templates');

require('lib/sass')(app);

app.use(express.static(path.join(__dirname, '../public')));
app.use(express.static(themePath + '/public'));
app.use(express.static(theme.getAdminTheme() + '/public'));
app.use(moduleManager.use('httpLog'));
app.use(moduleManager.use('request'));

// app.use(function(req, res, next) {
//     //res.writeHead(200, {'Content-Type': 'text/html; charset=UTF-8'});
//     next();
// });
app.use(favicon());
//app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session(sessionOptions));
app.use(moduleManager.use('localsMiddleware'));
app.use(moduleManager.use('templateTools'));
app.use(moduleManager.use('momentMiddleware'));

module.exports = app;