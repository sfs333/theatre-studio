module.exports.get = function(req, res, next) {
    res.render('page', {
        title: 'Settings',
        variables: {
            settingsForm: {form: 'site-settings-form'}
        }
    });
};

