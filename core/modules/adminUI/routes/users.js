var modules = require('modules');
var view = modules.use('views').get('user');
var mongoose = require('mongoose');
var User = mongoose.model('user');

module.exports.get = function(req, res, next) {
    res.dependency('adminUsers');
    view.render('adminList',
        { handler: 'table', theme: 'admin' },
        function (err, html) {
            if (err) return next(err);

            res.render('usersList', {
                title: 'ACCOUNTS',
                users: html
            });
        }
    );
};