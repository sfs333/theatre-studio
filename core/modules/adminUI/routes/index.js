var moduleManager = require('moduleManager');
var isAdmin = moduleManager.use('admin').middlewares.isAdmin;
var switchTheme = require('../index').middlewares.switchTheme;

module.exports = function(app) {
    app.get('/admin', isAdmin, switchTheme, require('./main').get);
    app.get('/admin/accounts', isAdmin, switchTheme, require('./users').get);
    app.get('/admin/accounts/view/:id', isAdmin, switchTheme, require('./userView').get);
    app.get('/admin/settings', isAdmin, switchTheme, require('./settings').get);
}