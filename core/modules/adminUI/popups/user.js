var moduleManager = require('moduleManager');
var popup = moduleManager.use('popup');
var form = moduleManager.use('form');

popup.add('adminUser', {
    template: 'popup/admin-user',
    theme: 'admin',
    params: {dialogClass: 'user-admin-popup-form', title: 'Account info', draggable: true},
    getVariables: function(context, cb) {
        form.createFormItem('user-admin-form', { id: (context.id || false), theme: 'admin' }, function(err, form) {
            if (err) return cb(err);

            form.render(function(err, html) {
                if (err) return cb(err);
                cb(err, {form: html});
            });
        });
    }
});