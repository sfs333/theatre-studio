module.exports = {
    pagination: false,
    fields: {
        itemLink: {
            title: 'Link',
            type: 'link',
            params: {
                title: 'go'
            }
        }
    }
};