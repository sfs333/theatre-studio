var async = require('async');
var mongoose = require('mongoose');

module.exports = function (context, cb) {
    var items = [];
    var user = context.user;
    if (!user)
        return cb(new Error('User not find!'));

    items.push({
        itemLink: {
            value: '#',
            attributes: {
                'data-id' : user.get('id'),
                'data-name' : 'popupUserTransactions',
                class: 'btn popup-btn link-user-edit'
            },
            params: {
                title: 'Transactions'
            }
        }
    });
    items.push({
        itemLink: {
            value: '#',
            attributes: {
                'data-id' : user.get('id'),
                'data-name' : 'popupUserPaystats',
                class: 'btn popup-btn link-user-edit'
            },
            params: {
                title: 'Paystats'
            }
        }
    });

    cb(null, {
        items: items,
        count: 5
    });
}
