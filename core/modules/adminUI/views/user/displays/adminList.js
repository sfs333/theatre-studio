var async = require('async');
var mongoose = require('mongoose');
var moduleManager = require('moduleManager');
var config = moduleManager.use('config');

module.exports = function (context, cb) {
    var User = mongoose.model('user');
    var self = this;
    var fieldAlias = {
        name: 'name',
        email: 'email',
        auth: 'auth',
        blocked: 'blocked',
        created: 'created',
        credits: 'credits'
    }
    async.parallel([
        function(cb) {
            User.count({'profile.chatName' : {$ne: 'admin'}}, cb);
        },
        function(cb) {
            var q = [
                {$match: {'profile.chatName' : {$ne: 'admin'}}},
                {$skip: ((context.page - 1) * self.pagination.count)},
                {$limit : self.pagination.count}
            ];
            q.push({$project: {
                name: '$profile.chatName',
                email: 1,
                auth: 1,
                blocked: 1,
                created: 1
            }});
            if (context.sort && context.sort.field && fieldAlias[context.sort.field]) {
                var sort = {};
                sort[fieldAlias[context.sort.field]] = context.sort.direction ? 1 : -1;
                q.push({ $sort : sort });
            }

            User.aggregate(q, cb);
        }
    ], function(err, results) {
        if (err) return cb(err);
        cb(null, {
            items: results[1],
            count: results[0]
            /** other options **/
        });
    });
}
