var modules = require('modules');
var log = modules.use('log')(module);
var Form = modules.use('form');
var formType = modules.use('formType');

var Info = require('./info');
Info.prototype = formType;
var info = new Info();

require('./events')(info);

Form.registerForm('system-actions-form', info);