module.exports = function() {
    this.name = 'user-admin-form';
    this.buttons = {
        submit: {title: 'Save'},
        cancel: false
    }
    this.getFields = function () {
        return {
            email: {
                type: 'email',
                title: 'User email',
                placeholder: 'Enter user email',
                required: true,
                activeValidate: false
            },
            //'profile.0.chatName': {
            //    type: 'text',
            //    title: 'User Screen Name',
            //    placeholder: 'Enter screen Name',
            //    required: true,
            //    activeValidate: false
            //},
            pass: {
                type: 'pass',
                title: 'Set Password',
                placeholder: 'Enter pass',
                required: true,
                activeValidate: false
            },
            //'profile.0.bday': {
            //    type: 'date',
            //    title: 'Birthday',
            //    placeholder: 'Enter Birthday',
            //    required: true
            //},
            //'profile.0.sex': {
            //    type: 'select',
            //    title: 'Gender',
            //    items: {
            //        male : 'Male',
            //        female : 'Female',
            //        couples : 'Couples',
            //        trans : 'Transexual'
            //    }
            //},
            blocked: {
                type: 'checkbox',
                title: 'Blocked',
                fieldClass: 'blocked-checkbox'
            }
        }
    }
}