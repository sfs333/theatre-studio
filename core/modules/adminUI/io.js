var moduleManager = require('moduleManager');
var async = require('async');
var mongoose = require('mongoose');
var User = mongoose.model('user');
module.exports = function(io) {
    io.on('connection', function (socket) {
        socket.on('deleteUser', function (info, cb) {
            if (!mongoose.isObjectId(info.id))
                return cb(new Error('id param not found'));

            async.waterfall([
                socket.request.loadUser,
                function(currentUser, cb) {
                    if (!currentUser.isAdmin() && !(currentUser.get('id') !== info.id))
                        return cb(new Error('Access denied!'));

                    User.findById(info.id, cb);
                },
                function(user, cb) {
                    if (user.isAdmin())
                        return cb(new Error('You cant delete Admin!'));
                    user.remove(cb);
                }
            ], function(err) {
                if (err)
                    return socket.request.error(err, cb);

                cb();
            });
        });

        socket.on('setPayStatus', function(info, cb) {
            if (!mongoose.isObjectId(info.id))
                return cb(new Error('id param not found'));

            async.waterfall([
                socket.request.loadUser,
                function(currentUser, cb) {
                    if (!currentUser.isAdmin())
                        return cb(new Error('Access denied!'));

                    User.findById(info.id, cb);
                },
                function(user, cb) {
                    var action = info.status ? 'payActive' : 'payDeActive';
                    user[action](cb);
                }
            ], function(err) {
                if (err)
                    return socket.request.error(err, cb);

                cb();
            });
        });
    });
}