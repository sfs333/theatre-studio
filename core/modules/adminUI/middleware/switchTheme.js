var modules = require('modules');
var path = require('path');
var user = require('mongoose').model('user');
var theme = modules.use('theme');
var config = modules.use('config');

module.exports = function(req, res, next) {
    var themeName = theme.getAdminTheme();
    res.locals.viewsPath = path.join(themeName, 'templates');
    res.locals.theme = {name: config.get('adminTheme'), path: themeName};
    next();
}