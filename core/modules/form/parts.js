var modules = require('modules');
var util = require('util');
var lib = require('./lib');
var async = require('async');

modules.use('parts').add('loadForm', {
    params: {},
    getHtml: function(context, cb) {
        async.waterfall([
            function(cb) {
                lib.createFormItem(context.formName, context, cb);
            },
            function(form, cb) {
                form.render(cb);
            }
        ], cb);

    }
});