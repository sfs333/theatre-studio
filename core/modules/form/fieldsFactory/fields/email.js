var util = require('util');
var Field = require('./field');
var validator = require('validator');

var Email = function(params, form) {
    this.construct(params, form);
    this.on('validate', function(field, callback) {
        if (!validator.isEmail(field.input))
            return callback(new Error('Email not correct'));

        callback();
    });
}

util.inherits(Email, Field);

module.exports = Email;