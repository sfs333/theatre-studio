var util = require('util');
var Field = require('./field');
var validator = require('validator');

var Markup = function(params, form) {
    this.construct(params, form);
}

util.inherits(Markup, Field);

var proto = Markup.prototype;
proto.set = function() {};
proto.get = function() { return null; };

module.exports = Markup;