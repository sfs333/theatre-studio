var util = require('util');
var Field = require('./field');

var Text = function(params, form) {
    this.construct(params, form);
    //this.on('validate', function(field, callback) {
    //    //if (!validator.isEmail(field.input))
    //        return callback(new Error('Text field error'));
    //
    //    callback();
    //});
}

util.inherits(Text, Field);

module.exports = Text;