var moduleManager = require('moduleManager');
var util = require('util');
var async = require('async');
var log = moduleManager.use('log')(module);
var ejsTools = moduleManager.use('ejsTools');
var templatesPath = 'form/fields';

var Field = function(params, form) {
    this.form = {};
    this.type = false;
    this.name = false;
    this.title = '';
    this.placeholder = '';
    this.description = '';
    this.required = false;
    this.activeValidate = false;
    this.fieldClass = false;
    this.input = null;
    this.template = false;
    this.multiple = false;
    this.listeners = {
        validate: []
    };
    this.classes = [];
}

var proto = Field.prototype;

proto.construct = function(info, form) {
    this.form = form;
    this.type = info.type;
    this.name = info.name;
    this.listeners = {};
    this.classes = ['form-item'];
    if (!this.template)
    this.template = this.type;

    if (info.title) this.title = info.title;
    if (info.placeholder) this.placeholder = info.placeholder;
    if (info.description) this.description = info.description;
    if (info.value) this.value = info.value;
    if (info.fieldClass)
        this.addClass(this.fieldClass = info.fieldClass);
    this.addClass('field-' + this.type);
    if (typeof info.required == 'boolean')
        this.required = info.required;
    if (typeof info.activeValidate == 'boolean')
        this.activeValidate = info.activeValidate;
    if (typeof info.multiple == 'boolean')
        this.multiple = info.multiple;

    if (this.multiple) {
       // this.template = this.template + '-multiple';
        this.classes.push('multiple');
        this.input = { add: [], edit: [], delete: []};
    }
}

proto.getClasses = function() {
    return this.classes.join(' ');
}

proto.addClass = function(name) {
    this.classes.push(name);
}

proto.set = function(v) {
    this.input = this.form.baseFilterString(util.format('%s', v));
}

proto.get = function() {
    return this.input;
}

proto.on = function(event, listener) {
    if (typeof listener !== 'function')
        return false;

    if (!this.listeners[event])
        this.listeners[event] = [];

    this.listeners[event].push(listener);
}

proto.emit = function(event, /*args...*/ callback) {
    var args = Array.prototype.slice.call(arguments);
    var callback = args.pop();

    if (typeof callback !== 'function')
        return console.error('Error execute emit - callback should be function type!');

    if (!this.listeners[event])
        return callback();

    async.applyEachSeries.apply(this, [this.listeners[event]].concat(args.slice(1)).concat(callback));
}

proto.getDefault = function() {
    return this.value;
}

proto.setDefault = function(value) {
   this.value = value;
}

proto.validate = function(callback) {
    var self = this;
    this.emit('validate', this, function(err) {
        if (err)
            err.field = self.name;
        callback(err, self.name);
    });
}

proto.prepareRemove = function(info, cb) {
    if (this.multiple)
        this.input.delete.push(info);
    else
        this.input = [];
    cb();
}

proto.render = function(cb) {
    this.fieldClass = this.fieldClass ? (this.fieldClass + ' ' + this.type) : this.type;
    ejsTools.read({
        path: (templatesPath + '/' + this.template),
        theme: this.form.theme,
        absolutePath: false,
        variables: {
            id: (this.form.name + '-' + this.name + '-field'),
            field: this
        }
    }, cb);
}

module.exports = Field;