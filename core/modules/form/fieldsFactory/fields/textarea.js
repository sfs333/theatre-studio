var util = require('util');
var Field = require('./field');
var validator = require('validator');

var Textarea = function(params, form) {
    this.rows = params.rows || 15;
    this.construct(params, form);
};

util.inherits(Textarea, Field);

var proto = Textarea.prototype;
proto.set = function(v) {
    this.input = validator.stripLow(validator.toString(v), true);
};

module.exports = Textarea;