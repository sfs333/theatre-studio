var util = require('util');
var Field = require('./field');

var Radios = function(info, form) {
    this.construct(info, form);
    this.items = info.items || {};
}

util.inherits(Radios, Field);
module.exports = Radios;