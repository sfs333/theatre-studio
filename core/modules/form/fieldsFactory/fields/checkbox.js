var util = require('util');
var Field = require('./field');

var Checkbox = function(params, form) {
    this.construct(params, form);
}

util.inherits(Checkbox, Field);
var proto = Checkbox.prototype;

proto.set = function(v) {
    this.input = Boolean(v);
}
module.exports = Checkbox;