var util = require('util');
var Field = require('./field');

var Number = function(params, form) {
    this.construct(params, form);
}

util.inherits(Number, Field);

module.exports = Number;