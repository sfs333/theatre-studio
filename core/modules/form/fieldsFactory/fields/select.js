var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var util = require('util');
var Field = require('./field');

var Select = function(info, form) {
    this.construct(info, form);
    this.items = info.items || {};
}

util.inherits(Select, Field);
module.exports = Select;