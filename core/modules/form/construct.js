var moduleManager = require('moduleManager');
var util = require('util');
var async = require('async');
var uuid = require('node-uuid');
var validator = require('validator');
var log = moduleManager.use('log')(module);
var objectStorage = moduleManager.use('objectStorage');
var fieldsFactory = moduleManager.use('formFieldFactory');
var ejsTools = moduleManager.use('ejsTools');
var config = moduleManager.use('config');
var formTemplate = 'form/layout';

var construct = function () {
    this.maxStrLenth = 200;
    this.clearTempData = function () {
        this.response = {status: 200, actions: {}};
        //this.activeValidate = false;
        this.isSubmit = false;
    }
    this.on = function (type, listener) {
        if (typeof this.listeners[type] != 'object')
            return log.warn('Don`t correct listener type for form builder');

        if (typeof listener == 'function')
            this.listeners[type].push(listener);
        else
            log.warn('Listener for action form should be is function');
    }
    this.validate = function (callback) {
        var form = this;
        async.waterfall([
            function(callback) {
                    var handlers = [];
                    for (var name in form.fields)
                        handlers.push(form.fields[name]);

                    async.eachSeries(handlers,
                        function (item, callback) {
                            item.validate.apply(item, [callback]);
                        },
                        callback);
            }, function(callback) {
                async.eachSeries(form.listeners.validate,
                    function (item, callback) {
                        item.apply(form, [callback]);
                    },
                    callback);
            }
        ], callback);
    }
    this.submit = function (cb) {
        this.emit('submit', cb);
    }
    this.emit = function (event, cb) {
        var self = this;
        async.eachSeries((self.listeners[event] || []),
            function (item, cb) {
                item.apply(self, [cb]);
            },
        cb);
    }
    this.createField = function(info) {
        return fieldsFactory.create(info, this)
    }
    this.createFields = function(info) {
        if (typeof info != 'object')
            return log.error('Cant create fields. Not correct info');

        var field;
        for (var name in info) {
            info[name].name = name;

            if (field = this.createField(info[name]))
                this.fields[name] = field;
        }
    }
    this.baseFilterString = function (str) {
        return str;
        str = validator.toString(str);
        if (str.length >= this.maxStrLenth);
        str = str.substr(0, this.maxStrLenth);
        str = validator.escape(str);
        return validator.stripLow(str);
    }
    this.build = function (model, params, callback) {
        var form = this;
        form.name = model.name;
        form.id = form.name + '-' + uuid.v4();
        form.createFields(model.getFields());
        form.clientParams = (typeof model.getClientParams == 'function') ? model.getClientParams() : {};
        if (typeof params.theme === 'string') {
            form.theme = params.theme;
            delete params.theme;
        }
        if (typeof params.socket === 'object') {
            form.socket = params.socket;
            delete params.socket;
        }
        for (var paramName in params) {
            this.values[paramName] = params[paramName];
        }
        for (var lName in model.listeners) {
            if (util.isArray(model.listeners[lName])) {
                model.listeners[lName].forEach(function(item) {
                    if (!form.listeners[lName])
                        form.listeners[lName] = [];
                    form.listeners[lName].push(item);
                });
            }
        }
        //this.listeners = model.listeners;

        if (typeof model.buttons == 'object') {
            for (var btnName in model.buttons)
                form.buttons[btnName] = model.buttons[btnName];
        }
        async.eachSeries(this.listeners.build,
            function (item, callback) {
                item.apply(form, [params, callback]);
            },
            function (err) {
                if (err) return callback(err);
                model.build.apply(form, [params, function (err) {
                    if (err) return callback(err);
                    objectStorage.set('form', form.id, form);
                    form.builded = true;
                    callback(err, form);
                }]);
            });
    }
    this.set = function(name, v) {
        var field = this.field(name);
        if (field) field.set(v);
    }
    this.get = function(name) {
        var field = this.field(name);
        if (field)
            return field.get(name);
    }
    this.field = function(name) {
        return this.fields[name] || false;
    }
    this.saveData = function (data, callback) {
        this.isSubmit = data.submit || false;
        this.values.params = {};
        for (var name in this.fields) {
            this.set(name, data.fields[name]);
        }
        if (data.params && (typeof data.params == 'object')) {
            for (var paramKey in data.params)
                this.values.params[paramKey] = data.params[paramKey];
        }
        callback(null);
    }

    this.destroy = function () {
        objectStorage.remove('form', this.id);
        delete this;
    }

    this.render = function (callback) {
        var form = this;
        var handlerCallbacks = [];
        var addItem = function (field) {
            handlerCallbacks.push(function (callback) {
                field.render.apply(field, [callback]);
            });
        }
        async.waterfall([
            function (callback) {
                if ((typeof form.fields) != 'object')
                    return callback(new Error('Form must have fields'));

                for (var name in form.fields)
                    addItem(form.fields[name]);
                async.parallel(handlerCallbacks, callback);
            },
            function(renderedFields, callback) {
                var attributes = '';
                for (var name in this.clientParams)
                    attributes += 'data-' + name + '="' + this.clientParams[name] + '" ';

                ejsTools.read({
                    path: formTemplate,
                    absolutePath: false,
                    theme: form.theme,
                    variables: {
                        form: form,
                        attributes: attributes,
                        fields: renderedFields
                    }
                }, callback);
            }

        ], callback);
    }
}

module.exports = construct;