var moduleManager = require('moduleManager');
var log = moduleManager.use('log');

var type = function() {
    this.on = function(type, listener) {
        if (typeof this.listeners != 'object')
            this.listeners = {};

        if (typeof this.listeners[type] != 'object')
            this.listeners[type] = [];

        if (typeof listener == 'function')
            this.listeners[type].push(listener);
        else
            log.warn('Listener for action form should be is function');
    }
    this.build = function(params, callback) {
        callback(null);
    }
}

module.exports = new type();