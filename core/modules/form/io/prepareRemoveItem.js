var modules = require('modules');
var objectStorage = modules.use('objectStorage');
var log = modules.use('log')(module);
var util = require('util');
var lib = require('./../lib');

module.exports = function (socket) {
    socket.on('prepareRemoveItem', function (info, cb) {
        log.info(util.format('Prepare remove value item from "%s" field', info.fieldName));
        var form;

        if (info.fieldName && info.formID && (form = lib.getFormItem(info.formID)))
            return form.fields[info.fieldName].prepareRemove(info, cb);

        cb(new Error('Not correct form'));
    });
}