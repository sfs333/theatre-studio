module.exports = function(io) {
    io.on('connection', function (socket) {
        require('./register')(socket);
        require('./process')(socket);
        require('./validate')(socket);
        require('./prepareRemoveItem')(socket);
        require('./dropFormItem')(socket);
        require('./disconnect')(socket);
    });
}