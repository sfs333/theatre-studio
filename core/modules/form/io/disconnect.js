var modules = require('modules');
var util = require('util');
var lib = require('./../lib');

module.exports = function(socket) {
    socket.on('disconnect', function () {
        if (!util.isArray(socket.userForms))
            return;

        socket.userForms.forEach(function(id) {
           lib.removeFormItem(id);
        });
    });
}