var modules = require('modules');
var util = require('util');
var lib = require('./../lib');

module.exports = function(socket) {
    socket.on('dropFormItem', function (data, cb) {
        var key;
        if (!util.isArray(socket.userForms) || !((key = socket.userForms.indexOf(data.id)) >= 0 ))
            return cb();

        lib.removeFormItem(data.id);
        delete socket.userForms[key];
    });
}