module.exports = function(socket) {
    socket.on('formRegister', function (data, cb) {

       if (!socket.userForms)
           socket.userForms = [];

        if (data.id)
            socket.userForms.push(data.id);
        cb();
    });
}