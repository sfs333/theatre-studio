var moduleManager = require('moduleManager');
var async = require('async');
var clc = require('cli-color');
var log = moduleManager.use('log')(module);
var objectStorage = moduleManager.use('objectStorage');
var lib = require('./../lib');

module.exports = function (socket) {
    socket.on('validateField', function (data, frontCallback) {
        var form;
        if (!data.id || !(form = lib.getFormItem(data.id)))
            return log.error('Don`t correct id form');

        if (!data.field)
            return log.error('Don`t correct info field for validate field');

        form.socket = socket;
        form.clearTempData();
        form.set(data.field, data.value);
        form.field(data.field).validate(function(err, field) {
            //var response = {status: 200, actions: {}};
            var actionName = 'formSuccessValidate';
            var actionInfo = {
                id: form.id,
                type: form.name,
                message: '',
                field: field
            }
            if (err) {
                //console.log(err);
                actionInfo.message = err.message || ('Error validate field ' + field);
                form.response.status = 403;
                actionName = 'formErrorValidate';
            }
            form.response.actions[actionName] = actionInfo;
            frontCallback(null, form.response);
        });


    });
}