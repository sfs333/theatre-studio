var construct = require('./construct');

var Item = function(model, params, callback) {
    this.id = null;
    this.name = 'sys-form';
    this.socket = null;
    this.theme = false;
    this.valid = true;
    this.fields = {};
    this.values = {};
    this.session = {};
    this.autoSaveSession = true;
    this.activeValidate = false;
    this.isSubmit = false;
    this.buttons = {
        submit: {title: 'Submit'},
        cancel: {title: 'Cancel'}
    }
    this.listeners = {
        validate: [],
        submit: [],
        build: []
    };
    this.response = {
        status: 200,
        actions: {}
    };
    this.build(model, params, callback);

}

Item.prototype = new construct();
module.exports = Item;