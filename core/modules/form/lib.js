var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var formItem = require('./item');
var objectStorage = moduleManager.use('objectStorage');

var lib = module.exports = {}
lib.createFormItem = function(formID, params, callback) {
    if ((typeof formID != 'string') || !objectStorage.isExist('formModel', formID)) {
        var err = new Error('Form "' + formID + '" not register in system, or not correct form id');
        log.error(err.message);
        return callback(err);
    }
    new formItem(objectStorage.get('formModel', formID), params, callback);
}

lib.registerForm = function (formID, model) {
    if (typeof model == 'object') {
        log.info('Register form - ' + formID);
        objectStorage.set('formModel', formID, model);
    } else {
        log.error('form model should be object type');
    }
}

lib.getForm = function (id) {
    return objectStorage.get('formModel', id);
}

lib.removeFormItem = function(id) {
    return objectStorage.remove('form', id);
}

lib.getFormItem = function(id) {
    return objectStorage.get('form', id);
}

//setInterval(function() {
//    if (objectStorage.storage.form) {
//        console.log("FORMS");
//        for (var id in objectStorage.storage.form) {
//            console.log(id);
//        }
//        console.log("***END****");
//    }
//
//}, 3000);