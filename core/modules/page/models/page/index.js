var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var mongoose = moduleManager.use('mongoose');
var Schema = mongoose.Schema;

var name = 'Page';
var opts = {};
var info = {
    authorID: require('./fields/authorID'),
    created: require('./fields/created'),
    updated: require('./fields/updated'),
    title: require('./fields/title'),
    body: require('./fields/body'),
    public: require('./fields/public')
};

var schema = new Schema(info, opts);
mongoose.model(name, schema);