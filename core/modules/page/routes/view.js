var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var mongoose = require('mongoose');
var util = require('util');
var Page = mongoose.model('Page');

module.exports.get = function (req, res, next) {
    var id = mongoose.Types.ObjectId(req.params.id);

    Page.findOne({_id: id, public: true}, function(err, page) {
        if (err) return next(err);

        if (!page) {
            var err = new Error('Page not found');
            err.status = 404;
            return next(err);
        }
        res.set({ 'content-type': 'text/html; charset=utf-8' });
        var variables = {};
        if (req.user.isAdmin())
            variables.add = util.format('<a href="/admin/content/edit/%s">edit</a>', page.get('id'));
        variables.body = page.get('body');
        res.render('page', {
            title: page.get('title'),
            variables: variables
        });
    });
};

