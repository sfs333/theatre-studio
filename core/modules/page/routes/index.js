var moduleManager = require('moduleManager');
var isAdmin = moduleManager.use('admin').middlewares.isAdmin;
var switchTheme = moduleManager.use('adminUI').middlewares.switchTheme;
var redisStorage = moduleManager.use('redisStorage');
var log = moduleManager.use('log')(module);
var view = require('./view').get;
var validator = require('validator');

module.exports = function(app, cb) {
    app.get('/admin/content', isAdmin, switchTheme, require('./list').get);
    app.get('/admin/content/edit/:id', isAdmin, switchTheme, require('./edit').get);
    app.get('/admin/content/add', isAdmin, switchTheme, require('./edit').get);

    app.get('/page/:id/', require('./view').get);

    cb();
}