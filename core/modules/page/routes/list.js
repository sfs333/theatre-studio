var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var views = moduleManager.use('views');



module.exports.get = function (req, res, next) {
    res.dependency('adminPageList');

    views.get('page').render('adminList',
        { handler: 'table', theme: 'admin' },
        function (err, html) {
            if (err) return next(err);

            res.render('page', {
                title: 'Content',
                variables: {add: '<a class="btn btn-primary link-page-add" href="/admin/content/add">CREATE</a>', html: html}
            });
        }
    );
};
