var mongoose = require('mongoose');
var Role = mongoose.model('Role');

Role.add({
    name: 'Guest',
    sysName: 'guest',
    description: 'Not auth users',
    module: 'permissions'
});
Role.add({
    name: 'Auth user',
    sysName: 'authUser',
    description: 'Auth users',
    module: 'permissions'
});