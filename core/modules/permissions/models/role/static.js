var mongoose = require('mongoose');
var util = require('util');
var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);

module.exports = function(staticMethods) {
    staticMethods.add = function(info, cb) {
        if (typeof cb !== 'function')
            cb = function(err) { if (err) log.error(err); }
        if ((typeof info !== 'object') || (!info.sysName))
            return cb(new Error('Not correct info for add role'));

        log.info(util.format('Register role "%s" for "%s" module'), info.sysName, info.module);
        var Role = mongoose.model('Role');
        Role.findOne({sysName: String(info.sysName)}, function(err, _role) {
            if (err) return cb(err);
            if (_role) return cb();
            var role = new Role(info);
            role.save(cb);
        });
    }
}