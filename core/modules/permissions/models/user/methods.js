module.exports = function(methods) {
    var access = require('./../../lib/access');
    methods.access = function(perm, cb) {
        access.check(perm, this, cb);
    }
}