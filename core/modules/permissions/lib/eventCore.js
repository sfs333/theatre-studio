var async = require('async');
var util = require('util');
var moduleManger = require('moduleManager');
var log = moduleManger.use('log')(module);

//var defCb = function(err) {
//    if (err) return log.error(err);
//}

var eventCore = function() {
    this.listeners = {};
};

var proto = eventCore.prototype;

proto.on = function(event, listener) {
    if (typeof event !== 'string')
        return log.error(new Error('Not correct 1 param (event name) in event Core'));

    if (!this.listeners[event] || !util.isArray(this.listeners[event]))
        this.listeners[event] = [];
    this.listeners[event].push(listener);
}

proto.emit = function(event, params, cb) {

}

module.exports = eventCore;