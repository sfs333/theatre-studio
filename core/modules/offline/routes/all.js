var modules = require('modules');
var validator = modules.use('validator');
var config = modules.use('config').get('offline');
var variables = modules.use('variables');
var pattern = '192.168.1.*';

module.exports = function(req, res, next) {
    variables.get('offline', function(err, value) {
        if (err || (value !== '1')) return next(err);

        if (validator.matches(req.getIP(), pattern.replace('*', '[1-9]{1,2}')))
            return next();

        res.status(403);
        res.render('offline');
    });
}