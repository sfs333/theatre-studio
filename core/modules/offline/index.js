module.exports.run = function(app, next) {
    require('./routes')(app);
    next();
}