var async = require('async');
var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);

var fields = {
    field: require('./fields/field'),
    date: require('./fields/date'),
    image: require('./fields/image'),
    boolean: require('./fields/boolean'),
    link: require('./fields/link'),
    string: require('./fields/string'),
    number: require('./fields/number'),
    markup: require('./fields/markup'),
    value: require('./fields/value'),
    navLink: require('./fields/navLink')
}

module.exports.create = function(name, info, view) {
    if (!info.type || !fields[info.type]) {
        log.warn('Not correct field type ' + info.type);
        return false;
    }
    info.name = name;
    return new fields[info.type](info, view);
}

