var util = require('util');
var Link = require('./link');

var NavLink = function(params, view) {
    this.construct(params, view);
    this.params.title = this.params.title || 'link';
}
util.inherits(NavLink, Link);

module.exports = NavLink;