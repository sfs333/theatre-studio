var util = require('util');
var Field = require('./field');
var moduleManager = require('moduleManager');
var ejsTools = moduleManager.use('ejsTools');

var Value = function(params, view) {
    this.construct(params, view);
}
util.inherits(Value, Field);
var proto = Value.prototype;
var parentRender = proto.render;

proto.render = function(cb) {
    //if (!this.params.template) {
    //    this.set('empty');
    //    return parentRender.apply(this, [cb]);
    //}
    //ejsTools.read({
    //    path: this.params.template,
    //    absolutePath: false,
    //    theme: this.theme,
    //    variables: {
    //        id: this.fields.id,
    //        field: this,
    //        fields: this.fields,
    //        view: this.view
    //    }
    //}, cb);


    cb(null, this.get());

}

module.exports = Value;