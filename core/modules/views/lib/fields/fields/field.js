var moduleManager = require('moduleManager');
var util = require('util');
var async = require('async');
var log = moduleManager.use('log')(module);
var ejsTools = moduleManager.use('ejsTools');
var templatesPath = 'views/fields';

var Field = function(params, view) {
    this.view = {};
    this.type = false;
    this.name = false;
    this.theme = false;
    this.title = '';
    this.prefix = false;
    this.suffix = false;
    this.fieldClass = false;
    this.value = null;
    this.template = '';
    this.params = {};
    this.attributes = {};
}

var proto = Field.prototype;

proto.construct = function(info, view) {
    this.view = view;
    this.type = info.type;
    this.name = info.name;
    this.template = templatesPath + '/' + this.type;
    this.attributes = {};

    if (info.title) this.title = info.title;
    if (info.prefix) this.prefix = info.prefix;
    if (info.suffix) this.suffix = info.suffix;
    if (info.value) this.value = info.value;
    if (info.fieldClass) this.fieldClass = info.fieldClass;

    this.saveParams(info.params);
}

proto.get = function() {
    if (!this.params || !this.params.get)
        return this.value || null;

    return this.params.get.apply(this, [this.value]);
}

proto.set = function(v) {
    return this.value = v;
}

proto.render = function(callback) {
    this.fieldClass = this.fieldClass ? (this.fieldClass + ' ' + this.type) : this.type;
    ejsTools.read({
        path: this.template,
        absolutePath: false,
        theme: this.theme,
        variables: {
            id: (this.view.name + '-' + this.name + '-field'),
            field: this
        }
    }, callback);
}

proto.saveParams = function(params) {
    this.params = {};
    if (!params || (typeof params !== 'object'))
        return false;

    var key;
    for (key in params)
        this.params[key] = params[key];
}

proto.renderAttrs = function() {
    var response = '';
    var name;
    for (name in this.attributes)
        response += ' ' + name + '="' + this.attributes[name] + '"';

    return response;
}

module.exports = Field;