var util = require('util');
var Field = require('./field');

var Number = function(params, view) {
    this.construct(params, view);
}
util.inherits(Number, Field);

var proto = Number.prototype;
var parentGetter = proto.get;
proto.get = function() {
    var value = parentGetter.apply(this);
    if (typeof value !== 'number')
        value = 0;
    if (typeof this.params.fixed != 'number')
        return value;

    return value.toFixed(this.params.fixed);
}

module.exports = Number;