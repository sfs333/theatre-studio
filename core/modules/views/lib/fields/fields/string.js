var util = require('util');
var Field = require('./field');

var String = function(params, view) {
    this.construct(params, view);
}
util.inherits(String, Field);
var proto = String.prototype;
var parentSet = proto.set;

proto.set = function(v) {
    if (this.params.trim)
        v = this.trim(v, this.params.trim);
    return parentSet.apply(this, [v]);
}

proto.trim = function(v, opt) {
    if (typeof opt !== 'object')
        opt = { length: 150 };
    var isBig = opt.length <= v.length;

    if (isBig) {
        v = v.substr(0, opt.length);
        if (opt.addEllipsis) v += '...';
    }

    return v;
}

module.exports = String;