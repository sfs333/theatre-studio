var util = require('util');
var Field = require('./field');

var Boolean = function(params, view) {
    this.construct(params, view);
    if (!this.params['true']) this.params['true'] = 'Yes';
    if (!this.params['false']) this.params['false'] = 'No';
}
util.inherits(Boolean, Field);
module.exports = Boolean;