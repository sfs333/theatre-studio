var async = require('async');
var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var config = moduleManager.use('config');
var ejsLocals = require('ejs-locals');
var app = require('app');
var util = require('util');
var path = require('path');
var _ = require('lodash');
var fields = require('./fields/index');
var uuid = require('node-uuid');
var templateLogging = config.get("ejsTools:logging");

var templatePath = 'views';
//uuid.v4()
//var View = function(model, info, cb) {
var View = function(name, info, views, cb) {
    this.name;
    this.idField;
    this.handler;
    this.pagination;
    this.displays;
    this.handlers;
    this.enableLayout;
    this.construct(name, info, views, cb);
};

var proto = View.prototype;

proto.construct = function(name, info, views, cb) {
    var self = this;
    self.enableLayout = (typeof info.info === 'boolean') ? info.info : true;
    async.waterfall([
         function(cb) {
             self.name = name;
             self.handler = info.handler || 'simple';
             self.fields = info.fields;
             self.pagination = info.pagination || {count: 10};
             self.displays = {};
             self.handlers = views.handlers;
             self.idField = info.idField || '_id';
             cb();
         }
    ], function(err) {
        if (err) return cb(err);
        cb(err, self);
    });
};

proto.makeFields = function(info, cb) {
    var name;
    var self = this;
    var workers = [];
    var addField = function(name, info) {
        workers.push(function(cb) {
            info.name = name;
            fields(info, self, function(err, field) {
                if (err) return cb(err);
                self.fields[name] = field;
                cb();
            });
        });
    }
    for (name in info)
        addField(name, info[name]);

    async.parallel(workers, cb);
};

proto.getViewData = function(context, handler, cb) {
    context = context || {};
    handler.apply(this, [context, function(err, info) {
        if (err)
            return cb(err);

        if (!info.items)
            return cb(new Error('Not correct response from handler'));
        if (!util.isArray(info.items))
            info.items = [info.items];

        cb(err, info);
    }]);
};

proto.getTemplatePath = function(subPath, theme) {
    if (typeof theme !== 'string')
        theme = config.get('theme');

    var filePath = path.join(app.get('basePath'), 'themes', theme, 'templates', (subPath + '.ejs'));
    if (templateLogging)
        log.info(filePath);
    return filePath;
};

proto.getPagination = function(count) {
    count = ((typeof count === 'number') && count > 0) ? count : 1;
    return Math.ceil(count / this.pagination.count);
};

proto.renderField = function(name, info, items, opt, cb) {
    var self = this;
    opt = opt || {};
    var field = fields.create(name, self.fields[name], self);
    if (!field)
        return log.warn('Not created ' + name + ' field for render');

    if ((typeof info !== 'object') || !info.value)
        info = { value: (typeof info === 'function' ? null : info) };

    field.set(info.value);
    if (info.attributes && (typeof info.attributes === 'object')) {
        var aKey;
        for (aKey in info.attributes)
            field.attributes[aKey] = info.attributes[aKey];
    }
    if (info.params && (typeof info.params === 'object')) {
        var pKey;
        for (pKey in info.params)
            field.params[pKey] = info.params[pKey];
    }

    field.fields = items;
    field.theme = opt.theme || false;
    field.render(function(err, html) {
        if (err) return cb(err);
        if (opt.line) {
            items[name] = html;
        } else {
            items[name] = {
                html: html,
                field: field
            };
        }
        cb();
    });
};

proto.renderItem = function(item, opt, cb) {
    var name;
    var itemFields = {};
    var workers = [];
    var self = this;
    opt = opt || {};

    //if (item.toJSON)
    //    item = item.toJSON();

    itemFields['id'] = item[self.idField] ? item[self.idField] : '0';
    var addFieldToRender = function(name, info, items) {
        workers.push(function(cb) {
            self.renderField(name, info, items, opt, cb);
        });
    };

    for(name in self.fields) {
        if ((typeof item[name] !== 'undefined') || (self.fields[name].type == 'markup'));
            addFieldToRender(name, item[name], itemFields);
    }

    async.series(workers, function(err) {
        cb(err, itemFields);
    });
};

proto.getRenderedFields = function(opt, handler, cb) {
    var opt = opt || {};
    opt.page = opt.page || 1;//(this.pagination.currentPage || 1);
    var items = [];
    var self = this;
    var workers = [];
    var theme = (typeof opt.theme === 'string') ? opt.theme : false;

    if (!opt.handler)
        opt.handler = self.handler;

    var addItemToRender = function(item) {
        workers.push(function(cb) {
            self.renderItem(item, {theme: theme }, function(err, itemFields) {
                if (err) return cb(err);
                items.push(itemFields);
                cb();
            });
        });
    };

    async.waterfall([
        function(cb) {
            self.getViewData(opt, handler, cb);
        },
        function(info, cb) {
            opt.count = info.count || 1;
            info.items.forEach(function(item) {
                addItemToRender(item);
            });
            async.series(workers, function(err) {
                cb(err);
            });
        },
        function(cb) {
            // self.pagination.currentPage = opt.page;
            cb();
        }
    ], function(err) {
        if (err) return cb(err);
        cb(null, items);
    });
};

proto.getRenderedContent = function(displayID, opt, cb) {
    var self = this;
    var handler = self.display(displayID);
    if (!handler)
        return cb(new Error('Not find display'));

    self.getRenderedFields(opt, handler, function(err, items) {
        if (err) return cb(err);

        var variables = {
            settings: {},
            items: items,
            fields: self.fields,
            enableLayout: false,
            page: (opt.page || 1)
        };
        ejsLocals(self.getTemplatePath((templatePath + '/' + 'handlers/' + opt.handler), opt.theme), variables, cb);
    });
};

proto.renderHandler = function(handler, opt, variables, cb) {
    handler = handler || 'simple';
    return this.handlers[handler](this, opt, variables, cb);
};

proto.render = function(displayID, opt, cb) {
    var self = this;
    var handler = self.display(displayID);
    if (!handler)
        return cb(new Error('Display ' + displayID + ' not find'));

    self.getRenderedFields(opt, handler, function(err, items) {
        if (err) return cb(err);

        var variables = {
            displayID: displayID,
            viewName: self.name,
            settings: {},
            items: items,
            fields: self.fields,
            pagesCount: self.getPagination(opt.count),
            enableLayout: self.enableLayout,
            page: 1,
            context: opt
        };

        self.renderHandler(opt.handler, opt, variables, cb);
        //ejsLocals(self.getTemplatePath((templatePath + '/' + 'handlers/' + opt.handler), opt.theme), variables, cb);
    });
};

proto.display = function(id, func) {
    if (typeof func == 'function')
        return this.displays[id] = func;

    return (this.displays[id] || false);
};

module.exports = View;