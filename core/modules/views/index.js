var modules = require('modules');
module.exports = require('./lib');

require('./parts');

module.exports.run = function (app, cb) {
    //app.use(require('./middleware/loadUser'));
    //require('./io/index')(app.get('io'));
    cb();
}

modules.use('publicDepends').addDependency({
    views: {
        scripts: ['js/views/index', 'js/views/actions'],
        dependencies: ['app', 'jq'],
        static: true
    }
});