var modules = require('modules');
var util = require('util');

modules.use('parts').add('loadView', {
    params: {},
    getHtml: function(context, cb) {
        var view = modules.use('views').get(context.view);
        if (!view)
            return cb(new Error(util.format('View %s not found', context.view)));

        view.getRenderedContent(context.display, context, cb);

    }
});