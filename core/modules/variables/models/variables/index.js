var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var mongoose = moduleManager.use('mongoose');
var Schema = mongoose.Schema;

var name = 'Variables';
var opts = {};
var info = {
        key: require('./fields/key'),
        value: require('./fields/value'),
        created: require('./fields/created')
}

var schema = new Schema(info, opts);
schema.index({ key: 1 });
mongoose.model(name, schema);