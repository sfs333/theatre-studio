var modules = require('modules');
var ejsLocals = require('ejs-locals');
var log = modules.use('log')(module);
var ejsTools = modules.use('ejsTools');
var util = require('util');
var async = require('async');
var path = require('path');

var templatePath = 'treeUlView/views';
module.exports = function (view, opt, variables, cb) {
    var items = {root: []};
    var key;
    var renderParams = {
        theme: opt.theme,
        path: path.join((opt.templatePath || templatePath), 'handlers', 'item')
    };

    for (key in variables.items) {
        var parent = variables.items[key].parent.field.get();
        if (!parent)
            parent = 'root';

        if (!items[parent])
            items[parent] = [];

        variables.items[key].parent = parent;
        variables.items[key].weight = variables.items[key].weight.field.get();
        variables.items[key].id = variables.items[key].id;
        items[parent].push(variables.items[key]);
    }
    //this.params.isDropdown
    var renderItem = function (fileData, item) {
        var childs = null;
        //console.log(util.format('ID: %s, Weight: %s, Parent: %s', item.id, item.weight, item.parent));
        if (opt.depth && items[item.id])
            childs = renderItems(fileData, items[item.id]);

        var vars = {
            items: item,
            id: item.id,
            weight: item.weight,
            parent: item.parent,
            childs: childs,
            depth: opt.depth
            //colWeight: (12 / (opt.colCount || 2))
        };

        delete vars.items.id;
        delete vars.items.weight;
        delete vars.items.parent;
        vars.colWeight = (12 / (opt.colCount || (Object.keys(vars.items).length - 1))).toFixed(0);

        return ejsTools.render(fileData, vars);
    }

    var compare = function (a, b) {
        return a.weight - b.weight;
    }

    var renderItems = function (fileData, items) {
        var key;
      //  if (opt.sort)
            items = items.sort(compare);
        var html = '';

        for (key in items)
            html += renderItem(fileData, items[key]);

        return html;
    }

    ejsTools.getFileData(renderParams, function (err, fileData) {
        if (err) return cb(err);

        variables.renderedList = renderItems(fileData, items.root);
        variables.name = opt.name;
        variables.autosubmit = opt.autosubmit;

        ejsLocals(view.getTemplatePath(util.format('%s/handlers/treeUl', (opt.templatePath || templatePath)), opt.theme), variables, cb);
    });

}