var modules = require('moduleManager');
var async = require('async');
var util = require('util');
//var path = require('path');
var log = modules.use('log')(module);

var treeView = require('./index');

module.exports = function(io) {
    io.on('connection', function (socket) {
        socket.on('treeViewProcess', function (data, cb) {
            var item;
            async.waterfall([
                function(cb) {
                    if (typeof data !== 'object')
                        return cb(new Error('not correct data for tree process io'));
                    if (!data.name)
                        return cb(new Error('not find "name" field in tree process io'));
                    if (!(item = treeView.get(data.name)))
                        return cb(new Error(util.format('not find tree view with name "%s"', data.name)));

                    item.save(data, cb);
                }
            ], function(err) {
                if (err)
                    return socket.request.error(err, cb);

                cb({ err: err });
            });
        });
    });
}