var async = require('async');
var modules = require('modules');
var util = require('util');
var views = modules.use('views');
var log = modules.use('log')(module);

ThreeView = function(name, params, cb) {
    this.name = '';
    this.listeners = {};
    this.params = {};
    this.getData = false;
    this.view = false;
    this.construct(name, params);
};

var proto = ThreeView.prototype;
var fn = function(err) {
    if (err) return log.error(err);
};

proto.construct = function(name, params) {
    this.name = name;
    this.params = params || {};
};

proto.save = function(info, cb) {
    if (typeof cb === 'function')
        cb();
};

proto.getViewName = function() {
    return util.format('treeView_%s', this.name);
};

proto.addView = function(additionalFields, cb) {
    if (typeof cb !== 'function')
        cb = fn;
    var self = this;
    var info = require('./../views/defaultInfo')();
    if (typeof additionalFields === 'object') {
        var key;
        for (key in additionalFields)
            info.fields[key] = additionalFields[key];
    }

    if (this.params.idField)
        info.idField = this.params.idField;

    views.add(this.getViewName(), info, function(err, view) {
        if (err) return cb(err);
        view.display('treeUl', require('./../views/displays/treeUl'));
        self.view = view;
        cb(null, view);
    });
};

proto.getView = function() {
    return views.get(this.getViewName());
};

proto.render = function(context, cb) {
    if (!this.getData)
        return cb(new Error('No setted getData handler for current three view. Cant render this.'));

    context.handler = context.handler || 'treeUl';
    context.name = this.name;
    context.depth = (typeof this.params.depth === 'boolean') ? this.params.depth : true;
    context.autosubmit = (typeof this.params.autosubmit === 'boolean') ? this.params.autosubmit : false;
    this.getView().render('treeUl', context, cb);
};


module.exports = ThreeView;