var Link = require('mongoose').model('Link');
var modules = require('modules');
var locals = modules.use('locals');
var _ = modules.use('underscore');

locals.link  = function (path, addSlash) {
    if (!_.isString(path))
        return '';

    var hasHttp = path.indexOf('http://') === 0;

    return Link.getAlias((!hasHttp && addSlash ? '/' : '') + path) || path;
};