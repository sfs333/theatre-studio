var modules = require('modules');
var ejsTools = modules.use('ejsTools');
var mongoose = require('mongoose');
var async = require('async');
var util = require('util');
var menuName = 'admin Menu';

var getRenderedAdminNav = function(cb) {
    var Menu = mongoose.model('Menu');
    var treeView = modules.use('treeView');
    async.waterfall([
        function(cb) {
        Menu.findOne({title: menuName}, cb);
    },
        function(menu, cb) {
            if (!menu) return cb(null, util.format('Not fond menu with name %s', menuName));

            treeView.get('menuView').render({
                menuId: menu.get('id'),
                templatePath: 'menu/views',
                menu: menu
            }, cb);
        },
        function(htmlMenu, cb) {
            ejsTools.read({
                path: 'adminNav',
                theme: 'admin',
                absolutePath: false,
                variables: {
                    adminMenu: htmlMenu
                }
            }, function(err, html) {
                // console.log(arguments);
                cb(err, html);
            });
        }
    ], cb);


}

module.exports.getRenderedAdminNav = getRenderedAdminNav;