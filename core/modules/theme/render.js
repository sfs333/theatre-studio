var mixin = require('utils-merge');

module.exports = function(app) {
    var orgR = function(name, options, fn) {
        var opts = {};
        var cache = this.cache;
        var engines = this.engines;
        var view;
        // support callback function as second arg
        if ('function' == typeof options) {
            fn = options, options = {};
        }

        // merge app.locals
        mixin(opts, this.locals);

        // merge options._locals
        if (options._locals) mixin(opts, options._locals);
        // merge options
        mixin(opts, options);

        // set .cache unless explicitly provided
        opts.cache = null == opts.cache
            ? this.enabled('view cache')
            : opts.cache;

        // primed cache
        if (opts.cache) view = cache[name];
        // view
        if (!view) {
            view = new (this.get('view'))(name, {
                defaultEngine: this.get('view engine'),
                root: (typeof options._locals.viewsPath === 'string' ? options._locals.viewsPath : this.get('views')),
                engines: engines
            });

            if (!view.path) {
                var err = new Error('Failed to lookup view "' + name + '" in views directory "' + view.root + '"');
                err.view = view;
                return fn(err);
            }

            // prime the cache
            if (opts.cache) cache[name] = view;
        }
        // render
        try {
            view.render(opts, fn);
        } catch (err) {
            fn(err);
        }
    };

    app.render = function(name, options, fn) {
        orgR.apply(app, [name, options, fn]);
    }
}