var modules = require('modules');
var path = require('path');
var user = require('mongoose').model('user');
var theme = modules.use('theme');
var config = modules.use('config');
var lib = require('./../lib');

module.exports = function(req, res, next) {
    if (!req.session || !(req.session.user === config.get('admin:id')))
        return next();


    lib.getRenderedAdminNav(function(err, html) {
        if (err) return next(err);

        res.locals.adminNav = html;
        next();
    });

}