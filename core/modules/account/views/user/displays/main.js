//picture name email sex country bday created
var async = require('async');
var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

module.exports = function (context, cb) {
    if (!context.id || !mongoose.isObjectId(context.id))
        return cb(new Error('Don`t correct user id in context view display'));

    var User = mongoose.model('user');
    var q = [
        {$match: {'_id' : ObjectId(context.id)}},
        {$limit : 1},
        {$project: {
            picture:'$profile.picture',
            name: '$profile.chatName',
            email: 1,
            sex: '$profile.sex',
            country: '$profile.country',
            bday: '$profile.bday',
            created: 1
        }}
    ];

    User.aggregate(q, function(err, users) {
        if (err) return cb(err);

        cb(null, {  items: users, count: 1 });
    });
}
