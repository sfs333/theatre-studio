var moduleManager = require('moduleManager');
var publicDepends = moduleManager.use('publicDepends');
require('./models');

module.exports.run = function (app, cb) {
    app.use(require('./middleware/loadUser'));
    require('./forms');
    require('./routes')(app);
    require('./io')(app);
    require('./views')(cb);

    //generate Users
   // require('./scripts/generateUsers')();
}

publicDepends.addDependency({
    accountForms: {
        scripts: ['js/account/registerForm', 'js/account/loginForm'],
        dependencies: ['form']
    },
    form: {dependencies: ['accountForms']},
    profilePage: {
        scripts: ['js/user/profile'],
        dependencies: ['app', 'jqTablesorter', 'jqUiTabs']
    }
});


