module.exports = function() {
    this.name = 'user-form';
    this.buttons = {
        submit: {title: 'Save'},
        cancel: false
    }
    this.getFields = function() {
        return {
            email: {
                type: 'email',
                title: 'Your email',
                placeholder: 'Enter email',
                required: true,
                activeValidate: true
            },
            pass: {
                type: 'pass',
                title: 'Set New Password',
                placeholder: 'Enter pass',
                description: 'not required'
            }
        }
    }
}