var async = require('async');
var passMinLength = 7;
var messages = {
    noMatch: 'Passwords do not match',
    shortLength: 'Pass should contain more '+ passMinLength + ' symbols',
    required: 'Field is required',
    noAgree: 'You must agree to the terms of the agreement.'
}
module.exports = function(fields) {
    //Password
    fields.pass.on('validate', function(field, callback) {
        var pass = field.get();
        var retype = field.form.fields.rep_pass.get();
        if (!pass)
            return callback(new Error(messages.required));
        if (retype && (pass != retype))
            return callback(new Error(messages.noMatch));
        if (String(pass).length < passMinLength)
            return callback(new Error(messages.shortLength));
        callback();
    });
    fields.rep_pass.on('validate', function(field, callback) {
        var pass = field.form.fields.pass.get();
        var retype = field.get();
        if (pass && (pass != retype))
            return callback(new Error(messages.noMatch));
        if (String(retype).length < passMinLength)
            return callback(new Error(messages.shortLength));
        callback();
    });
    //Agree field
    fields.agree.on('validate', function(field, callback) {
        if (!field.get())
            return callback(new Error(messages.noAgree));

        callback(null);
    });
}