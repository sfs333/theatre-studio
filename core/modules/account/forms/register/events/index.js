module.exports = function(mf) {
    mf.formType.on('build', function(params, cb) {
        var self = this;
        this.socket.request.sessionManager.get(function(err, session) {
           if (err) return cb(err);
           if (session.user) {
               delete self.buttons.submit;
               self.fields = {
                   infoText: self.createField({
                       type: 'markup',
                       name: 'infoText',
                       title: '',
                       value: 'You is auth now'
                   })
               };
               return cb();
           }

            require('./validate')(self.fields);
            require('./submit')(self);
            cb();
        });
    });
}