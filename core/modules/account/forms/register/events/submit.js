var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var ejsTools = moduleManager.use('ejsTools');
var config = moduleManager.use('config');
var mailConfirm = moduleManager.use('mailConfirm');
var confirmRout = moduleManager.use('account', '/routes/confirmRegistration');

module.exports = function(form) {
    //Send confirmation email
    form.on('submit', function(callback) {
        var host;
        if ((typeof this.socket != 'object') || !(host = this.socket.handshake.headers.host))
            return callback(new Error('Impossible get domain name from socket'));

        var info = {
            to: this.get('email'),
            subject: config.get('siteDomain') + ' - confirm registration',
            html: ''
        };
        ejsTools.read({
            path: 'messages/register-confirm-email',
            absolutePath: false,
            theme: false,
            variables: {
                name: this.get('name'),
                mail: this.get('email'),
                domain: host,
                supportEmail: config.get('supportEmail')
            }
        }, function(err, html) {
            if (err) return log.error(err);
            info.html = html;
            mailConfirm.add(info, 'account', function(err, data) {
                if (err) return log.error(err);
                log.info('Message send to user - ' + info.to);
                console.log(data);
            }, confirmRout.get);
        });

        callback(null);
    });
    //Show message
    form.on('submit', function(callback) {
        var form = this;
        ejsTools.read({
            path: 'messages/before-register-message',
            absolutePath: false,
            theme: false,
            variables: {}
        }, function(err, html) {
            if (err)
                return callback(err);
            form.response.actions.submitRegisterForm = {html: html};
            callback(null);
        });
    });

    mailConfirm.mailEvents.use('noDataaccount', function(callback) {
        this.req.session.confirmationAccount = {confirm: false};
        this.res.redirect('/');
    });
}