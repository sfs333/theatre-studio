module.exports = function() {
    this.name = 'user-register-form';
    this.getClientParams = function () {
        return {
            'error-wrapper-selector': '.form-register.form .warning'
        };
    };
    this.getFields = function () {
        return {
            email: {
                type: 'email',
                title: 'Email',
                placeholder: 'Enter email',
                required: true,
                activeValidate: true,
                //description: ''
            },
            'profile.0.chatName': {
                type: 'text',
                title: 'Login',
                placeholder: 'Enter your login',
                required: true,
                activeValidate: true,
                //description: ''
            },
            pass: {
                type: 'pass',
                title: 'Password',
                placeholder: 'Enter pass',
                required: true,
                activeValidate: true
            },
            rep_pass: {
                type: 'pass',
                title: 'Confirm Password',
                placeholder: 'Repeat pass',
                required: true,
                activeValidate: true
            },
            'profile.0.bday': {
                type: 'date',
                title: 'Birthday',
                placeholder: 'Enter Birthday',
                required: true
            },
            'profile.0.sex': {
                type: 'select',
                title: 'Gender',
                items: {
                    male : 'Male',
                    female : 'Female'
                    //couples : 'Couples',
                    //trans : 'Transexual'
                }
            },
            agree: {
                type: 'checkbox',
                title: 'I have read and agree to the terms and conditions',
                fieldClass: 'form-item-agree'
            }
        }
    }
}