var moduleManager = require('moduleManager');
var util = require('util');
var async = require('async');
var fs = require('fs');
var path = require('path');

var names = ['Anna', 'Jenny', 'Lenny', 'Sweetty', 'Carry', 'Helen',
    'Jenny', 'Lana', 'kITTY', 'Jennufer', 'Marta', 'Marta', 'Lorry',
    'Lynnete', 'Gabriela', 'Sofi', 'Elly', 'Tinky', 'Daisy', 'Pam',
    'Minnie', 'Hunny', 'Sunny', 'Katty', 'Sheila', 'Ursula', 'Danny',
    'May', 'Abby', 'Caroline', 'Abigale', 'lessy', 'pytty', 'merry',
    'bob', 'kitnes', 'jonn', 'leon', 'bonn', 'leddy', 'mike', 'linda',
    'sny', 'anyledy'
];
var sexes = ['male', 'female', 'couples', 'trans'];
var countries = ['us', 'uk', 'ru', 'pl', 'fr', 'it'];
var pass = '1111111';
var email = '%d.mem@mail.com';
var userDefTemplate = '%d_User';
var picturePathTemplate = "files/user-pictures/%d.jpg";
var start = 0;
var finish = 500;
var pictureCount = 15;

function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var generateUsers = function() {

    var User = require('mongoose').model('user');
    var systemPath = path.normalize(__dirname + '/../../../public');
    var log = moduleManager.use('log')(module);
    var nLength = names.length - 1;
    var sLength = sexes.length - 1;
    var cLength = countries.length - 1;
    var handlers = [];
    if (!finish) finish = nLength;

    log.info(util.format('Generate %d Users', (finish - start)));

    var handler = function(num) {
        var userEmail = util.format(email, num);
        handlers.push(function(cb) {
            async.waterfall([
                function(cb) {
                    User.findOne({email: userEmail}, cb);
                },
                function(user, cb) {
                    if (user)
                        return cb(new Error(util.format('Geniration Users: user with email %s already exists', userEmail)));

                    log.info(util.format("Generate user %s", userEmail));
                    var user = new User({
                        "pass": pass,
                        "email": userEmail,
                        "blocked": false,
                        "auth": false
                    });
                    user.profile.push({
                         "sex": sexes[getRandom(0, sLength)],
                         "chatName": (names[num] ? names[num] : util.format(userDefTemplate, num)),
                         "country": countries[getRandom(0, cLength)]
                    });
                    user.broadcastSession.push({
                        "params": {"guests": true},
                        "isActive": false,
                        "type": "free_chat"
                    });
                    var userPicturePath = util.format(picturePathTemplate, getRandom(1, pictureCount));
                    var picturePath = path.join(systemPath, userPicturePath);
                    fs.exists(picturePath, function(exist) {
                        if (exist)
                            user.profile[0].picture = userPicturePath;
                        user.save(cb);
                    });
                }, function(user, st, cb) {
                    log.info(util.format('user %s success added! Email: %s, Pass: %s', user.get('name'), user.get('email'), pass));
                    cb(null);
                }
            ], cb);

        });
    }

    for (var num = start; num <= finish; num++) {
        handler(num);
    }
    async.parallel(handlers, function(err) {
        if (err) return log.error(err);
    });
}

module.exports = function(timeout) {
    setTimeout(generateUsers, (timeout || 2000));
}
