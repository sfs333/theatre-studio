var crypto = require('crypto');

module.exports = function(methods) {
    methods.encryptPassword = function(password) {
        return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
    }
    methods.checkPassword = function(password) {
        return this.encryptPassword(password + '') == this.hashedPassword;
    }
    methods.isAuth = function() {
        return Boolean(this.get('auth'));
    }
}