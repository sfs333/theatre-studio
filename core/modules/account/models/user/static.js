var mongoose = require('mongoose');

module.exports = function(staticMethods) {
    staticMethods.createGuest = function() {
        var User = mongoose.model('user');
        var info = {auth: false, profile: [{'chatName' : 'guest'}]};
        var user = new User(info);
        return user;
    }
    staticMethods.authorize = function(id, callback) {
        if (!id || !mongoose.isObjectId(id))
            return callback(null, this.createGuest());

        mongoose.model('user')
            .findById(id, function(err, user) {
                if (err) return callback(err);
                if (!user) return callback(null, staticMethods.createGuest());

                callback(null, user);
        });
    }
    staticMethods.authorization = function(user, callback) {
        if (!user || (typeof user != 'object'))
            return callback(new Error('Not correct model user'));

        user.set('blocked', false);
        user.set('auth', true);
        user.save(function(err, user) {
            callback(err, user);
        });
    }
    staticMethods.getter = function(value, cb) {
        if (typeof cb !== 'function')
            cb = function(err) { if (err) console.error(err); }
        if (value instanceof this)
            return cb(null, value);
        if (mongoose.isObjectId(value))
            return this.findById(value, cb);

        cb(new Error('User not find!'));
    }
}