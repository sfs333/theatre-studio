module.exports = function(schema) {
    schema.virtual('name')
        .get(function() {
            var profile = this.getProfile();
            return profile ? (profile.get('chatName')) : 'no name';
        })
        .set(function(v) {
            return this.getProfile().set('chatName', v);
        });
}
