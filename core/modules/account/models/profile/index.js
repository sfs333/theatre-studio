var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var mongoose = moduleManager.use('mongoose');
var Schema = mongoose.Schema;

var User = mongoose.model('user');

var info = {
    picture: require('./fields/picture'),
    chatName: require('./fields/chatName'),
    description: require('./fields/description'),
    sex: require('./fields/sex'),
    bday: require('./fields/bday'),
    country: require('./fields/country'),
    age: require('./fields/age'),
    languages: require('./fields/languages')
}

User.schema.plugin(function (schema, options) {
    var profileSchema = new Schema(info);
    schema.add({ profile: [profileSchema] });
    require('./static')(schema.statics);
    require('./methods')(schema.methods);
    require('./virtual')(schema);
    mongoose.rebuildModel('user', schema);
});