module.exports.get = function(req, res, next) {
    if (req.user.isAuth())
        return  res.redirect('/');

    res.render('page', {
        title: 'Login',
        variables: {
            loginForm: {form: 'user-login-form'}
        }
    });
};
