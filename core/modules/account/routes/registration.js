module.exports.get = function(req, res) {
    if (req.user.isAuth())
        return  res.redirect('/');

    res.render('page', {
        title: 'Registration',
        variables: {
            loginForm: {form: 'user-register-form'}
        }
    });
};
