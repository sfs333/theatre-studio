var moduleManager = require('moduleManager');
var async = require('async');
var User = require('mongoose').model('user');

module.exports.get = function(info, req, res, next) {
    if (!info || !info.to)
        return next();

    async.waterfall([
        function(callback) {
            User.findOne({email: info.to, blocked: true}, callback);
        },
        function(user, callback) {
            if (!user) return callback(new Error('User not exist'));
            User.authorization(user, callback);
        },
        function(user, callback) {
            req.session.user = user.get('id');
            res.locals.user = user.toObject();
            callback();
        }
    ], function(err) {
        if (err) return next(err);

        req.session.confirmationAccount = {confirm: true};
        res.redirect('/');
    });
}