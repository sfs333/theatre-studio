module.exports.get = function(req, res, next) {

    if (!req.user.isAuth()) {
        var err = new Error('Access denied');
        err.status = 403;
        return next(err);
    }
    req.session.destroy();
    req.user.auth = false;
    req.user.save(function(err, user) {
        if (err) return next(err);
        console.log('log out user ' + user.get('name'));
        res.redirect('/');
    });
};
