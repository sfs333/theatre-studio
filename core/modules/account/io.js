var moduleManager = require('moduleManager');

module.exports = function(app) {
    var io = app.get('io');
    var User = require('mongoose').model('user');
    io.use(function(socket, next) {
        socket.request.loadUser = function(callback) {
            socket.request.sessionManager.get(function(err, session) {
                if (err) return callback(err);
                User.authorize(((session && session.user) ? session.user : false), callback);
            });
        }

        next(null);
    });
}