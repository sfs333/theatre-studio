var moduleManager = require('moduleManager');
var user = require('mongoose').model('user');

module.exports = function(req, res, next) {
    user.authorize(req.session.user, function(err, user) {
        if (err) return next(err);

        req.user = res.locals.user = user;
        next();
    });
}