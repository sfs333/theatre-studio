var moduleManager = require('moduleManager');
var util = require('util');
var log = moduleManager.use('log')(module);
var objectStorage = moduleManager.use('objectStorage');
var mailConfirm = moduleManager.use('mailConfirm');

module.exports.get = function(req, res, next) {
    var id = util.format('%s', req.params.id);
    var type = util.format('%s', req.params.type);
    var data = objectStorage.get('mailConfirm', id);

    if (data && (typeof data.callback == 'function')) {
        log.info('Confirmation from ' + (data.info.to || 'no-email'));
        data.callback(data.info, req, res, next);
        return objectStorage.remove('mailConfirm', id);
    }

    var event = mailConfirm.mailEvents.getEventName('noData', type);
    if (mailConfirm.mailEvents.getListeners(event).length > 0) {
        mailConfirm.mailEvents.emit(event, {req: req, res: res, next: next})
        return;
    }

   log.warn('Error confirmation from email');
   res.redirect('/');
};