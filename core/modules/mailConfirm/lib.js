var modules = require('modules');
var validator = require('validator');
var config = modules.use('config');
var objectStorage = modules.use('objectStorage');
var log = modules.use('log')(module);
var serialEE = modules.use('serialEE');

var mail = require('modules').use('mail');
var mailEvents = new serialEE();
var replacePattern = '[confirm-path]';

mailEvents.getEventName = function(event, type) {
    return String(event) + String(type);
}

module.exports.add = function(info, type, callbackSend, callbackConfirm) {
    if (!type || (typeof type != 'string'))
        type = 'default';

    log.info('Send Mail Confirmation to ' + info.to);
    var data = {callback: callbackConfirm, info: info};
    var path = '/mail-confirm/' + type + '/' + objectStorage.set('mailConfirm', false, data);
    log.info('Confirm path - ' + path);

    if (typeof info.html == 'string')
        info.html = info.html.replace(replacePattern, path);

    mail.send(info, callbackSend);
    mailEvents.emit(mailEvents.getEventName('send', type));
}

module.exports.replacePattern = replacePattern;
module.exports.mailEvents = mailEvents;