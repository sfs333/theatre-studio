var moduleManager = require('moduleManager');
var publicDepends = moduleManager.use('publicDepends');
var config = moduleManager.use('config');

module.exports = function(req, res, next) {
    var jsCollections = [];
    var render = res.render;

    res.locals.scripts = [];
    res.locals.jsFileName = false;


    res.render = function(file, options, fn) {
        if (config.get('optimizeJS:enable')) {
            publicDepends.getJS(jsCollections, function(err, fileName) {
                if (err)
                    console.error(err);
                else
                    res.locals.jsFileName = fileName;

                render.apply(res, [file, options, fn]);
            });
        } else {
            res.locals.scripts = publicDepends.getScripts(jsCollections);
            render.apply(res, [file, options, fn]);
        }
    }

    res.dependency = function(name) {
        var dep;

        if (!(dep = publicDepends.getDependency(name)))
            return false;

        jsCollections.push(name);
    }

    next(null);

}