var modules = require('modules');
var switchTheme = modules.use('adminUI').middlewares.switchTheme;
var isAdmin = modules.use('admin').middlewares.isAdmin;

module.exports = function(app) {
    app.get('/admin/feedback/messages', isAdmin, switchTheme, require('./adminList').get);
    app.get('/admin/feedback', isAdmin, switchTheme, require('./adminSettings').get);
}