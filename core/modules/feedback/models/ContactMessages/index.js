var modules = require('modules');
var log = modules.use('log')(module);
var mongoose = modules.use('mongoose');
var Schema = mongoose.Schema;

var name = 'ContactMessages';
var opts = {};
var info = {
    created: require('./fields/created'),
    title: require('./fields/title'),
    name: require('./fields/name'),
    contact: require('./fields/contact'),
    message: require('./fields/message'),
    ip: require('./fields/ip')
}

var schema = new Schema(info, opts);
mongoose.model(name, schema);