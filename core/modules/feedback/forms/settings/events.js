var modules = require('modules');
var async = require('async');
var variables = modules.use('variables');

module.exports = function(info) {
    info.on('validate', function (cb) {
        var self = this;
        this.socket.request.loadUser(function(err, user) {
            if (err) return cb(err);

            if (!user.isAdmin)
                return cb(new Error('Access denied'));
            cb();
        });
    });

    info.on('build', function(params, cb) {
        var self = this;
        async.parallel([
            function(cb) {
                variables.get('feedbackSave', function(err, value) {
                    if (err) return cb(err);
                    if (value)
                        self.field('save').setDefault(value === '1');
                    cb();
                });
            },
            function(cb) {
                variables.get('feedbackReceivers', function(err, value) {
                    if (err) return cb(err);
                    if (value)
                        self.field('receivers').setDefault(value);
                    cb();
                });
            },
            function(cb) {
                variables.get('feedbackTemplate', function(err, value) {
                    if (err) return cb(err);
                    if (value)
                        self.field('template').setDefault(value);
                    cb();
                });
            }
        ], cb);
    });

    info.on('submit', function (cb) {
        variables.set('feedbackSave', this.get('save'), cb);
    });

    info.on('submit', function (cb) {
        var receivers = this.get('receivers');
        if (receivers) return variables.set('feedbackReceivers', receivers, cb);
        cb();
    });

    info.on('submit', function (cb) {
        var template = this.get('template');
        if (template) return variables.set('feedbackTemplate', template, cb);
        cb();
    });
}