module.exports = function() {
    this.name = 'feedback-settings-form';
    this.buttons = {
        submit: {title: 'Save'},
        cancel: false
    }
    this.getFields = function() {
        return {
            save: {
                type: 'checkbox',
                title: 'Save copy',
                description: 'Save copy on the site',
                fieldClass: 'toggle-checkbox'
            },
            receivers: {
                type: 'text',
                title: 'Receivers'
            },
            template: {
                type: 'textarea',
                title: 'Message template',
                description: 'variables: <%- name %>, <%- contact %>, <%- message %>, <%- ip %>, <%- siteName %>'
            }
        }
    }
}