var modules = require('modules');
var validator = require('validator');
var async = require('async');
var util = require('util');
var ejs = require('ejs');
var mail = modules.use('mail');
var variables = modules.use('variables');

module.exports = function(mf) {
    mf.formType.on('build', function(context, cb) {
        var self = this;
        variables.get('feedbackSave', function(err, value) {
            if (err) return cb(err);
            self.mf.saveChanges = Boolean(value === '1');
            cb();
        });
    });
    mf.formType.on('submit', function(cb) {
        if (!this.socket.handshake || !this.socket.handshake.address)
            return cb();
        var ip = this.socket.handshake.address.replace(/[\: f]/g, '');
        if (ip)
            this.mf.document.set('ip', ip);
        cb();
    });
    mf.formType.on('submit', function(cb) {
        var self = this;
        async.parallel([
            function(cb) { variables.get('feedbackReceivers', cb) },
            function(cb) { variables.get('feedbackTemplate', cb) },
            function(cb) { variables.get('siteName', cb) }

        ], function(err, info) {
            if (err) return cb(err);
            mail.send({
                to: info[0],
                subject: util.format('Feedback from %s - %s', info[2], self.get('title')),
                html: ejs.render(info[1], {
                    name: self.get('name'),
                    contact: self.get('contact'),
                    message: self.get('message'),
                    ip: self.get('ip'),
                    siteName: info[2]
                })
            }, cb);
        });
    });
    mf.formType.on('submit', function(cb) {
        this.response.actions.closePopup = {};
        this.response.actions.popupMessage = {
            title: 'Messege success send',
            text: 'You success send message for admin',
            footer: true
        };
        cb();
    });
}