module.exports = function () {
    this.name = 'contact-us-form';
    this.buttons = {
        submit: {title: 'Submit'},
        cancel: {title: 'Cancel'}
    }
    this.getFields = function () {
        return {
            title: {
                type: 'text',
                title: false,
                placeholder: 'Enter title',
                required: true
            },
            name: {
                type: 'text',
                title: false,
                placeholder: 'Enter your name',
                required: true
            },
            contact: {
                title: false,
                type: 'email',
                placeholder: 'Enter your email',
                required: true
            },
            message: {
                type: 'textarea',
                title: 'Message',
                placeholder: 'Enter your message',
                required: true
            }
        }
    }
}