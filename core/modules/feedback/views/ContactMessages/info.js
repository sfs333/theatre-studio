module.exports = {
    pagination: {
        count: 15
    },
    fields: {
        title: {
            title: 'Title',
            type: 'string',
            params: {}
        },
        created: {
            title: 'Created',
            type: 'date',
            params: {}
        },
        message: {
            type: 'string',
            title: 'message',
            params: {
                trim: {length: 40, addEllipsis: true}
            }
        },
        name: {
            type: 'string',
            title: 'User Name',
            params: {}
        },
        contact: {
            type: 'string',
            title: 'Contacts',
            params: {}
        },
        ip: {
            type: 'string',
            title: 'ip',
            params: {}
        },
        actions: {
            title: 'Actions',
            type: 'markup',
            params: {
                template: 'views/partial/actions',
                actions: [{
                    title: 'VIEW',
                    path: '#',
                    class: 'popup-marker',
                    data: { name: 'contactMessagePopup', title: 'Message' }
                },
                {
                    title: 'DELETE',
                    path: '/admin/feedback/message/%id/delete',
                    class: 'popup-form-marker',
                    data: { 'form-name': 'admin-delete-feedback' }
                }]
            }
        }
    }
};