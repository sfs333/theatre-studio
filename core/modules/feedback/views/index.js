var modules = require('modules');
var async = require('async');

module.exports = function (cb) {
    async.parallel([
        function(cb) {
            modules.use('views').add('ContactMessages', require('./ContactMessages'), function(err, view) {
                if (err) return cb(err);
                    view.display('adminList', modules.use('viewsTools').mgDisplay({
                            modelName: 'ContactMessages',
                            sort: 'created'
                    }));
                cb();
            });
        }
    ], cb);
}