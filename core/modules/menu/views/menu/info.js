module.exports = {
    pagination: {
        count: 15
    },
    fields: {
        title: {
            title: 'Title',
            type: 'string',
            params: {}
        },
        created: {
            title: 'Created',
            type: 'date',
            params: {}
        },
        name: {
            type: 'string',
            title: 'System Name',
            params: {}
        },
        type: {
            type: 'string',
            title: 'Type',
            params: {}
        },
        addToLayout: {
            type: 'boolean',
            title: 'In Layout',
            params: {}
        },
        actions: {
            title: 'Actions',
            type: 'markup',
            params: {
                template: 'views/partial/actions',
                actions: [{
                    title: 'ITEMS',
                    path: '/admin/menu/%id/items',
                    class: 'link-menu-items'
                },
                {
                    title: 'EDIT',
                    path: '/admin/menu/%id',
                    class: 'popup-form-marker link-menu-edit',
                    data: { 'form-name': 'admin-menu-form' }
                },
                {
                    title: 'DELETE',
                    path: '/admin/menu/%id',
                    class: 'popup-form-marker',
                    data: { 'form-name': 'admin-delete-menu' }
                }]
            }
        }
    }
};