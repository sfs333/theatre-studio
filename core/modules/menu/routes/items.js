var modules = require('modules');
var util = require('util');
var validator = require('validator');
var async = require('async');
var mongoose = require('mongoose');
var Menu = mongoose.model('Menu');

module.exports.get = function(req, res, next) {
    var menuId = req.params.id;
    var parentMenu = null;
    if(!validator.isMongoId(menuId))
        return next();
    async.waterfall([
        function(cb) {
            Menu.findOne(menuId, cb);
        },
        function(menu, cb) {
            if (!menu)
                return cb(new Error('Menu not found'));
            parentMenu = menu;
            cb();
        },
        function(cb) {
            modules.use('treeView')
                .get('menuItems')
                .render({menuId: menuId, theme: 'admin'}, cb);
        }
    ], function(err, html) {
        if (err) return next(err);

        res.render('page', {
            title: util.format('Items for %s', parentMenu.get('title')),
            variables: {add: '<a href="/admin/menu' + menuId + '/items" class="popup-form-marker btn btn-primary link-menu-item-add" data-theme="admin" data-form-name="admin-menu-item-form" data-id="" data-menu-id="' + menuId + '" >CREATE</a>', html: html}
        });

    });
};