var modules = require('modules');
var log = modules.use('log')(module);
var views = modules.use('views');

module.exports.get = function (req, res, next) {
    views.get('menu').render('adminList',
        { handler: 'table', theme: 'admin' },
        function (err, html) {
            if (err) return next(err);

            res.render('page', {
                title: 'Menu list',
                variables: {add: '<a href="/admin/menu" class="popup-form-marker btn btn-primary link-menu-add" data-title="Create Menu" data-theme="admin" data-form-name="admin-menu-form" data-id="" >CREATE</a>', html: html}
            });
        }
    );
};
