var modules = require('modules');
var switchTheme = modules.use('adminUI').middlewares.switchTheme;
var isAdmin = modules.use('admin').middlewares.isAdmin;

module.exports = function(app, cb) {
    app.get('/admin/menu', isAdmin, switchTheme, require('./menu').get);
    app.get('/admin/menu/:id', isAdmin, switchTheme, require('./menu').get);
    app.get('/admin/menu-list', isAdmin, switchTheme, require('./list').get);
    app.get('/admin/menu/:id/items', isAdmin, switchTheme, require('./items').get);
    cb();
}