var async = require('async');
var modules = require('modules');
var publicDepends = modules.use('publicDepends');

require('./models');
require('./forms');
module.exports = require('./lib');
module.exports.run = function(app, cb) {
    require('./treeViews');
    app.use(require('./middleware/loadMenus'));
    async.series([
        function(cb) {
            require('./routes')(app, cb);
        },
        function(cb) {
            require('./views')(cb);
        }
    ], cb);
}

publicDepends.addDependency({
    adminMenuList: {
        scripts: ['js/admin/menu/list'],
        dependencies: ['app']
    }
});

//jqUiDroppable