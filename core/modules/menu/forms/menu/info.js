module.exports = function () {
    this.name = 'admin-menu-form';
    this.buttons = {
        submit: {title: 'save'},
        cancel: {title: 'back'}
    }
    this.getFields = function () {
        return {
            title: {
                type: 'text',
                title: 'Title',
                placeholder: 'Enter title',
                required: true
            },
            type: {
                type: 'select',
                title: 'Type',
                items: { vertical: 'Vertical menu', horizontal: 'Horizontal menu' }
            },
            description: {
                type: 'textarea',
                title: 'Description',
                placeholder: 'Enter description'
            },
            addToLayout: {
                type: 'checkbox',
                title: 'Add to Layout auto load'
                //fieldClass: 'form-item'
            }
        }
    }
}