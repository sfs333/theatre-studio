var modules = require('modules');
var views = modules.use('views');

module.exports = function(mf) {
    mf.formType.on('build', function(params, cb) {
        this.on('validate', function(cb) {
            var self = this;
            this.socket.request.loadUser(function(err, user) {
                if (err) return cb(err);

                if (!user.isAdmin)
                    return cb(new Error('Access denied'));
                cb();
            });
        });
        cb();
    });

    mf.formType.on('postSave', function(cb) {
        if (this.values.isPopUp) {
            var self = this;
            views.get('menu').renderItem(this.mf.document, {theme: self.theme, line: true}, function(err, items) {
                if (err) return cb(err);
                var id = items.id || '';
                delete items.id;
                self.response.actions.addViewRow = {
                    id: id,
                    isNew: self.mf.document.isNew,
                    items: items,
                    view: 'menu'
                };
                self.response.actions.closePopup = {};
                cb();
            });
        } else {
            this.response.actions.redirect = {url: 'admin/menu-list'};
            cb();
        }
    });
}