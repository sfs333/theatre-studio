var modules = require('modules');
var views = modules.use('views');
var mongoose = require('mongoose');
var util = require('util');
var Menu = mongoose.model('Menu');
var validator = modules.use('validator');

module.exports = function(mf) {
    mf.formType.on('build', function(params, cb) {
        var self = this;
        Menu.find({}, {title: 1}, function(err, menus) {
            if (err) return cb(err);

            menus.forEach(function(menu) {
                self.field('menu').items[menu.get('id')] = menu.get('title');
            });
            cb();
        });
    });

    mf.formType.on('build', function(params, cb) {
        var self = this;

        self.field('menu').setDefault(self.values.menuId);
        cb();
    });

    mf.formType.on('build', function(params, cb) {
        this.field('menu').set = function(v) {
            this.input = validator.isMongoId(v) ? mongoose.Types.ObjectId(v) : null;
        }
        cb();
    });

    mf.formType.on('submit', function(cb) {
        //this.response.actions.closePopup = {};
        this.response.actions.redirect = {url: util.format('admin/menu/%s/items', this.get('menu'))};
        cb();
    });
}