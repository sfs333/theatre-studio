var modules = require('modules');
var mongoose = require('mongoose');
var MenuItems = mongoose.model('MenuItems');
var validator = require('validator');
var util = require('util');
var async = require('async');

module.exports = function(info, cb) {
    var items = util.isArray(info.items) ? info.items : [];
    var workers = [];
    var addToUpdate = function(item) {
        if (!item.id || !validator.isMongoId(item.id))
            return false;

        workers.push(function(cb) {
            var params = {
                parent: validator.isMongoId(item.parent) ? mongoose.Types.ObjectId(item.parent) : (item.parent || 'root'),
                weight: item.weight ? Number(item.weight) : 0
            };
            MenuItems.update({_id: mongoose.Types.ObjectId(item.id)}, params, cb);
        });
    };
    items.forEach(function(item) {
        addToUpdate(item);
    });
    async.parallel(workers, cb);
}