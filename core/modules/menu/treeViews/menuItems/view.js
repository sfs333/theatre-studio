module.exports = {
    path: {
        title: '',
        type: 'link',
        params: {
            title: 'title'
        }
    },
    actions: {
        title: 'Actions',
        type: 'markup',
        params: {
            template: 'menu/menu-items-actions'
        }
    }
}