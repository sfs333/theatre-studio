var modules = require('modules');
var log = modules.use('log')(module);

var menuItems = modules.use('treeView').add('menuItems', {});
menuItems.getData = require('./getData');
menuItems.save = require('./save');
menuItems.addView(require('./view'));