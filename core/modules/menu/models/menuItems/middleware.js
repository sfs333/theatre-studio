var modules = require('modules');
var validator = modules.use('validator');
module.exports = function(schema) {

    schema.pre('save', function (next) {
        this.external = validator.contains(this.path, 'http://');
        next();
    });
}