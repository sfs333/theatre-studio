var modules = require('modules');
var mongoose = require('mongoose');
var async = require('async');
var treeView = modules.use('treeView');
var Menu = mongoose.model('Menu');

var renderMenus = function(menus, cb) {
    var workers = [];
    var renderedMenus = {};
    var addWorker = function(menu) {
        workers.push(function(cb) {
            treeView.get('menuView').render({
                menuId: menu.get('id'),
                templatePath: 'menu/views',
                menu: menu
            },
                function(err, html) {
                if (err) return cb(err);

                renderedMenus[menu.get('name')] = html;
                cb();
            });
        });
    }
    menus.forEach(function(menu) {
        addWorker(menu);
    });
    async.parallel(workers, function(err) {
        if (err) return cb(err);

        cb(null, renderedMenus);
    });
}

var renderLayoutMenus = function(cb) {
    async.waterfall([
        function(cb) {
            Menu.find({addToLayout: true}, cb);
        },
        renderMenus
    ], cb);
}

module.exports.renderLayoutMenus = renderLayoutMenus;