var modules = require('modules');

var assoc = {
    '/pogibshie-poety' : '/etudy/pogibshie-poety',
    "/cinderella" : "/spectacly/zolushka",
    "/christmas-oranges" : "/spectacly/novogodniye-apelisiny",
    "/sleep-beauty" : "/spectacly/spachaya-crasavica",
    "/queen-new-year" : "/spectacly/snezjnaya-koroleva",
    "/pushkin" : "/etudy/pushkin",
    "/grandma" : "/etudy/o-dedushkah-i-babushkah",
    "/laydi-layda" : "/etudy/laydi-layda",
    "/leady-mery" : "/etudy/leady-sovershenstvo",
    "/women-about-war" : "/etudy/zjenschiny-o-voine",
    "/vse-mogut-coroli" : "/etudy/vse-mogut-coroli",
    "/Chees" : "/etudy/dyrky-v-syre",
    "/summer-colors" : "/meropriyatiya/krasky-leta",
    "/girl-day" : "/meropriyatiya/zjenskiy-den",
    "/photo-presentation" : "/videos/photo-presentation"
};

module.exports = function (app) {
    app.use(function (req, res, next) {
        console.log(req.url);
        
        if (assoc[req.url])
            return res.redirect(assoc[req.url]);


        next();
    });
};






