module.exports = {
    pagination: {
        count: 100
    },

    fields: {
        title: {
            title: 'Title',
            type: 'string',
            params: {}
        },
        path: {
            title: 'Path',
            type: 'string',
            params: {}
        },
        created: {
            title: 'Created',
            type: 'date',
            params: {}
        },
        actions: {
            title: 'Actions',
            type: 'markup',
            params: {
                template: 'views/partial/actions',
                actions: [
                    {
                        title: 'EDIT',
                        path: '/admin/meta-tag/%id',
                        class: 'popup-form-marker',
                        data: {'form-name': 'admin-meta-tag-form'}
                    },
                    {
                        title: 'DELETE',
                        path: '/admin/meta-tag/%id',
                        class: 'popup-form-marker',
                        data: {'form-name': 'admin-meta-tag-delete-form'}
                    }
                ]
            }
        }
    }
};