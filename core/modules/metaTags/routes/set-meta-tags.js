var MetaTag = require('mongoose').model('MetaTag');
var async = require('async');
var _ = require('underscore');
// var Link = require('mongoose').model('Link');

module.exports = function (req, res, done) {

    res.locals.metatags = {
        title: '',
        description: '',
        keywords: ''
    };

    var pushMeta = function (meta) {
        if (meta.title) res.locals.metatags.title += meta.title + ' ';
        if (meta.description) res.locals.metatags.description += meta.description + ' ';
        if (meta.keywords) res.locals.metatags.keywords += meta.keywords + ', ';
    };

    var clear = function (text) {
        text = text.replace(/ {2,}/g, ' ');
        text = text.replace(/,{2,}/g, ',');
        text = text.replace(/^ {0,}| {0,}$/g, '');
        text = text.replace(/^,{0,}|,{0,}$/g, '');

        return text;
    };

    var clearMetaTags = function () {
        res.locals.metatags.title = clear(res.locals.metatags.title);
        res.locals.metatags.description = clear(res.locals.metatags.description);
        res.locals.metatags.keywords = clear(res.locals.metatags.keywords);
    };

    async.series([
        function (next) {
            MetaTag.findOne({path: req.path}, function (err, customMetaTag) {
                if (customMetaTag)
                    pushMeta(customMetaTag);

                next();
            });
        },
        function (next) {
            MetaTag.findOne({path: '*'}, function (err, commonMetaTag) {
                if (commonMetaTag)
                    pushMeta(commonMetaTag);

                next();
            });
        },
        function (next) {
            clearMetaTags();
            next();
        }

    ], done);

};

