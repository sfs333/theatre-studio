var modules = require('modules');
var isAdmin = modules.use('admin').middlewares.isAdmin;
var switchTheme = modules.use('adminUI').middlewares.switchTheme;

module.exports = function (app, cb) {

    app.use(require('./set-meta-tags'));
    app.get('/admin/meta-tag-list', isAdmin, switchTheme, require('./admin-list').get);

    cb();
};