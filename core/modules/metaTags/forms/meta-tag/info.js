module.exports = function () {
    this.name = 'admin-meta-tag-form';
    this.buttons = {
        submit: {title: 'save'},
        cancel: {title: 'back'}
    };

    this.getFields = function () {

        return {
            path: {
                type: 'text',
                title: 'Path',
                placeholder: 'Enter the path',
                required: true
            },
            title: {
                type: 'text',
                title: 'Title tag',
                placeholder: 'Enter the title tag'
            },
            description: {
                type: 'textarea',
                title: 'Description',
                placeholder: 'Enter the description',
                rows: 7
            },
            keywords: {
                type: 'textarea',
                title: 'Key-words',
                placeholder: 'Enter the key-words',
                rows: 7
            }
        }
    };
};