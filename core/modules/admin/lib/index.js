var moduleManager = require('moduleManager');
var config = moduleManager.use('config');
var adminID = config.get('admin:id');
var mongoose = require('mongoose');

if (!mongoose.isObjectId(adminID))
    adminID = false;

module.exports.getAdminId = function() {
    return adminID;
}