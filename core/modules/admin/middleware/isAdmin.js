var moduleManager = require('moduleManager');

module.exports = function(req, res, next) {
    if (!req.user || !req.user.isAdmin()) {
        var err = new Error('Access denied');
        err.status = 403;
        return next(err);
    }
    next();
}