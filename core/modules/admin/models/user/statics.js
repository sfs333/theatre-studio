var moduleManager = require('moduleManager');
var lib = require('moduleManager').use('admin', '/lib');
var adminID = lib.getAdminId();

module.exports = function(statics) {
    statics.getAdmin = function(callback) {
        if (!adminID)
            return callback(new Error('Not find admin:id in config file'));

        this.findById(adminID, callback);
    }
}