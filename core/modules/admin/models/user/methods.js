var moduleManager = require('moduleManager');
var lib = require('moduleManager').use('admin', '/lib');
var adminID = lib.getAdminId();

module.exports = function(methods) {
    methods.isAdmin = function() {
        return (this.get('id') === adminID);
    }
}