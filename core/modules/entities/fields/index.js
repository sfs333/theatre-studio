var fields = {};

var add = function(type, field) {
    if (typeof type !== 'string')
        return false;
    if (typeof field !== 'object')
        return false;

    fields[type] = field;
}

var get = function(type) {
    if (typeof type !== 'string')
        return false;

    return fields[type] || false;
}

add('text', require('./text'));

module.exports.add = add;
module.exports.get = get;