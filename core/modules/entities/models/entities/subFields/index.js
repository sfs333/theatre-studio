module.exports = {
    name: require('./fields/name'),
    title: require('./fields/title'),
    description: require('./fields/description'),
    created: require('./fields/created'),
    weight: require('./fields/weight'),
    opt: require('./fields/opt')
};