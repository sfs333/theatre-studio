var modules = require('modules');
var mongoose = require('mongoose');
var util = require('util');
var async = require('async');
var lib = require('./index');
var ModelEntities = mongoose.model('Entities');
var fields = require('./../fields');


var Entity = function(name, info) {
    if (!this.validateName(name))
        return new Error('Not correct name');

    this.name = name;
    this.title = '';
    this.description = '';
    this.fields = {};

    this.build(typeof info === 'object' ? info : {});
}

var proto = Entity.prototype;

proto.build = function(info) {
    this.title = info.title || this.name.replace('_', ' ');
    if (info.description)
        this.description = info.description;
}

proto.buildModel = function(cb) {

    console.log('Build model for ' + this.get('name'));

    cb();
}
proto.get = function(field) {
    return this[field];
}

proto.set = function(field, v) {
    if (this.get(field) !== 'undefined')
        this[field] = v;
}

proto.clearName = function(str) {
    return (new RegExp("[a-z0-9_]{1,}", 'g').exec(str)[0]);
}

proto.validateName = function(str) {
    return this.clearName(str) === str;
}


proto.docSetFields = function(doc) {
    doc.set('title', this.title);
    doc.set('description', this.description);
    return doc;
}
proto.save = function(doc, cb) {
    if (arguments[0] === 'function')
        var cb = arguments[0];

    if (doc && (typeof doc.save === 'function')) {
        this.docSetFields(doc);
        return doc.save(cb);
    }
    var self = this;
    async.waterfall([
        function(cb) {
            self.getDoc({}, {'_id' : 1}, cb);
        },
        function(doc, cb) {
            return self.docSetFields(doc || ModelEntities({name: self.name})).save(cb);
        },
        function(doc, n, cb) {
            cb(null, self);
        }
    ], cb);
}

proto.fieldIsExist = function(info, cb) {

    this.getDoc({'fields.name' : info.name}, {_id: 1}, function(err, res) {
        if (err) return cb(err);
        cb(null, (Boolean(res)));
    });
}

proto.getDoc = function(conditions, params, cb) {
    if (typeof arguments[0] === 'function')
        var cb = arguments[0];
    if (typeof arguments[1] === 'function')
        var cb = arguments[1];
    else if (typeof conditions !== 'object')
        var conditions = {};

    conditions.name = this.get('name');
    ModelEntities.findOne(conditions, ((typeof params === 'object') ? params : null), cb);
}

proto.attachField = function(type, info, opt, cb) {
    var field = fields.get(type);
    if (!field)
        return cb(new Error(util.format('Not exist type field for entities.', type)));
    var self = this;
    var save = Boolean(opt.save);
    delete opt.save;
    async.waterfall([
        function(cb) {
            self.fieldIsExist(info, cb);
        },
        function(exist, cb) {
            if (exist)
                return cb(new Error(util.format('Field %s already exist', info.name)));

            self.getDoc(cb);
        },
        function(doc, cb) {
            doc.fields.push({
                name: info.name,
                title: info.title || '',
                description: info.description || '',
                weight: info.weight || 1,
                opt: opt
            });
            if (save)
                return self.save(doc, cb);

            cb();
        }
    ], cb);
}

module.exports = Entity;