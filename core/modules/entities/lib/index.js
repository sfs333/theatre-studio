var modules = require('modules');
var Entity = require('./Entity');
var validator = require('validator');
var mongoose = require('mongoose');
var ModelEntities = mongoose.model('Entities');
var async = require('async');

var make = function(name, info) {
    return new Entity(validator.stripLow(String(name)), info);
}

var build = function(infoDoc, cb) {

    var name = infoDoc.get('name');
    if (!name)
        return cb(new Error('Name not correct in entity'));

    make(name, {title: infoDoc.get('title'), description: infoDoc.get('description')}).buildModel(cb);
}
var buildAll = function(cb) {
    var workers = [];
    var addWorker = function(infoDoc) {
        workers.push(function(cb) {
            build(infoDoc, cb);
        });
    };
    async.waterfall([
        function(cb) {
            ModelEntities.find({}, cb);
        },
        function(infoDocs, cb) {
            if (!infoDocs) return cb();

            infoDocs.forEach(function(infoDoc) {
                addWorker(infoDoc);
            });

            async.parallel(workers, cb);
        }
    ], cb);

}

module.exports.make = make;
module.exports.buildAll = buildAll;