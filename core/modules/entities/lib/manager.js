var async = require('async');
var mongoose = require('mongoose');
var util = require('util');
var validator = require('validator');
var ModelEntities = mongoose.model('Entities');
var EntitiesClass = require('./Entities');

var Manager = function() {};

var proto = Manager.prototype;

proto.clearName = function(str) {
    return (new RegExp("[a-z0-9_]{1,}", 'g').exec(str)[0]);
}
proto.validateName = function(str) {
    return this.clearName(str) === str;
}

proto.add = function(info, cb) {

    if (!info || (typeof info !== 'object'))
        return cb(new Error('Info for build entities not found.'));
    if (!info.name)
        return cb(new Error('info.name for build entities not found.'));
    if (!this.validateName(info.name))
        return cb(new Error('info.name not valid. Only a-z, 0-9 and _'));

    var opt = {name: info.name};
    async.waterfall([
        function(cb) {
            ModelEntities.findOne({ name: opt.name }, {'_id' : 1}, cb)
        },
        function(exist, cb) {
            if (exist)
                return cb(new Error(util.format('Entity %s already exist in system.', opt.name)));

            opt.title = info.title || info.name.replace('_', ' ');
            if (info.description)
                opt.description = info.description;
            var entitiesInfo = new ModelEntities(opt);
            entitiesInfo.save(cb);
        },
        function(doc, n, cb) {
            new EntitiesClass(doc, cb);
        }
    ], cb);
}

proto.get = function(name, cb) {
    if (!name)
        return cb(new Error('name param not found.'));

    async.waterfall([
        function(cb) {
            ModelEntities.findOne({ name: name }, cb)
        },
        function(doc, cb) {
            if (!doc)
                return cb(new Error(util.format('entity %s param not found.', name)));
            new EntitiesClass(doc, cb);
        }
    ], cb);
}

module.exports = Manager;