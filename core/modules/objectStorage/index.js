var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var uuid = require('node-uuid');

module.exports = {

    storage: {},
    maxTypeId: 70,

    showCount: function(type) {
//        log.info('"' + type + '" changed. Count: ' + Object.keys(this.storage[type]).length);
    },

    isExist: function(type, id, forse) {
        var forse = forse || false;
        return ((typeof this.storage[type] == 'object') && (this.storage[type][id] || forse));
    },

    get: function(type, id) {
        if (this.isExist(type, id))
            return this.storage[type][id];

        return null;
    },

    set: function(type, id, object) {

        if (typeof type != 'string') {
            log.error('Param "type" should be "string" type');
            return false;
        }

        if (type.length > this.maxTypeId) {
            log.error('Param "type" should be shorter than ' + this.maxTypeId);
            return false;
        }

        if (typeof id != 'string')
            id = uuid.v4();

        if (id.length > this.maxTypeId) {
            log.error('Param "id" should be shorter than ' + this.maxTypeId);
            return false;
        }

        if (typeof this.storage[type] != 'object')
            this.storage[type] = {}

        this.storage[type][id] = object;
//        this.showCount(type);
        return id;
    },

    remove: function(type, id) {
        if (this.isExist(type, id, true)) {
            delete this.storage[type][id];
//            this.showCount(type);
            return true;
        }

        return false;
    }
};

