var moduleManager = require('moduleManager');
var publicDepends = moduleManager.use('publicDepends');

module.exports.run = function(app, callback) {
    callback(null);
}

publicDepends.addDependency({
    cvAnimate: {
        scripts: ['js/canvasAnimation/canvas', 'js/canvasAnimation/index']
    }
});