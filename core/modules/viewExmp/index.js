var moduleManager = require('moduleManager');
var User = require('mongoose').model('user');
var log = moduleManager.use('log')(module);
var views = moduleManager.use('views');
var async = require('async');

var infoExMenu = {
    fields: {
        title: {
            title: 'Title',
            type: 'string',
            params: {}
        },
        link: {
            title: 'url',
            type: 'link',
            params: {
                title: 'title'
            }
        }
    }
}

var infoUserList = {
    fields: {
        email:{
            title: 'Email',
            type: 'string',
            params: {}
        },
        _id:{
            title: 'ID',
            type: 'string',
            params: {}
        },
        created: {
            title: 'Register User date',
            type: 'date',
            params: {}
        },
        blocked: {
            title: 'Blocked',
            type: 'boolean',
            params: {}
        }
    }
};

var infoPagesList = {
    pagination: {
        count: 2
    },
    fields: {
        path:{
            title: 'URL',
            type: 'string',
            params: {}
        },
        title:{
            title: 'Title',
            type: 'string',
            params: {}
        },
        created: {
            title: 'Created date',
            type: 'date',
            params: {}
        },
        public: {
            title: 'Publish now',
            type: 'boolean',
            params: {}
        }
    }
};

var infoSesList = {
    pagination: {
        count: 5
    },
    fields: {
        name:{
            title: 'Name',
            type: 'string',
            params: {}
        },
        picture:{
            title: 'Picture',
            type: 'image',
            params: {}
        },
        online:{
            title: 'Is online',
            type: 'boolean',
            params: {}
        },
        email: {
            title: 'Email',
            type: 'string',
            params: {}
        },
        sex: {
            title: 'Sex',
            type: 'string',
            params: {}
        },
        bday: {
            title: 'Birthday',
            type: 'date',
            params: {}
        }
    }
};

module.exports.run = function (app, next) {
    require('./routes/index')(app);

    async.parallel([
        function(cb) {
            views.add('exMenu', infoExMenu, cb);
        },
        function(cb) {
            views.add('userList', infoUserList, cb);
        },
        function(cb) {
            views.add('pagesList', infoPagesList, cb);
        },
        function(cb) {
            views.add('sesList', infoSesList, cb);
        }
    ], next);
}