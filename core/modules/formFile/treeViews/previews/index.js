var modules = require('modules');
var log = modules.use('log')(module);

var filePreviews = modules.use('treeView').add('filePreviews', {depth: false, autosubmit: true});
filePreviews.getData = require('./getData');
filePreviews.save = require('./save');
filePreviews.addView(require('./fileView'));


var imagePreviews = modules.use('treeView').add('imagePreviews', {depth: false, autosubmit: true});
imagePreviews.getData = require('./getData');
imagePreviews.save = require('./save');
imagePreviews.addView(require('./imageView'));

