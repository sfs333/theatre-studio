var modules = require('modules');
var mongoose = require('mongoose');
//var MenuItems = mongoose.model('MenuItems');
var validator = require('validator');
var util = require('util');
var async = require('async');
var form = modules.use('form');

module.exports = function(info, cb) {
    var itemForm,
        field,
        context = info.context;

    if (!util.isArray(info.items) || (info.items.length === 0))
        return cb();

    if (!context.form || !(itemForm = form.getFormItem(context.form)))
        return cb(new Error(util.format('Form %s not found', context.form)));

    if (!context.field || !(field = itemForm.field(context.field)))
        return cb(new Error(util.format('Field %s not found in form %s', context.field, context.form)));

    info.items.forEach(function(item) {
        field.changeFile(item.id, {
            weight: item.weight
        });
    });

    cb();
}