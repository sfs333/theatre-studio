module.exports = {
    publicPath: {
        type: 'markup',
        params: {
            get: function(v) {
                return require('util').format(
                    '<a href="/%s" class="popup-img-marker" data-separate="true"><img src="/%s" height="60" /></a>',
                    (this.params.full || v), v
                );
            }
        }
    },
    name: {
        type: 'string',
        title: 'Name'
    },
    size: {
        type: 'string',
        title: 'Size',
        suffix: ' Mb',
        params: {
            get: function(v) {
                return (v / 1048576).toFixed(2);
            }
        }
    },
    type: {
        type: 'string',
        title: 'File Type',
        params: {}
    },
    uploaded: {
        type: 'date',
        title: 'Uploaded',
        params: {
            format: 'LLLL'
        }
    },
    actions: {
        title: 'Actions',
        type: 'markup',
        params: {
            template: 'views/partial/actions',
            actions: [
                {
                    title: '',
                    path: '#',
                    class: 'remove glyphicon glyphicon-remove'
                }
            ]
        }
    }
}