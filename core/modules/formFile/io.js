var modules = require('modules');
var objectStorage = modules.use('objectStorage');
var log = modules.use('log')(module);
var ss = require('socket.io-stream');
var form = modules.use('form');

module.exports = function(io) {
    io.on('connection', function(socket) {
        ss(socket).on('file', function(stream, info, callback) {
            log.info('Loading file from stream...');
            var itemForm = false;
            if (info.fieldName && info.formID && (itemForm = form.getFormItem(info.formID)))
                return itemForm.fields[info.fieldName].upload(stream, info, callback);
            
            callback(new Error('Not correct form'));

        });
    });
}

