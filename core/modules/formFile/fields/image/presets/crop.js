var cp = module.exports = function(info) {
    this.width = info.width || 640;
    this.height = info.height || 480;
    this.mp = info.middlePoint || {x:false, y:false};
}

var proto = cp.prototype;
proto.pre = function(cb) {
    cb();
}
proto.make = function(info, cb) {
    if (!this.mp.x) this.mp.x = (info.size.width / 2);
    if (!this.mp.y) this.mp.y = (info.size.height / 2);

    var x = this.mp.x - (this.width / 2);
    var y = this.mp.y - (this.height / 2);
    info.img.crop(
        this.width,
        this.height,
        ((x >= 0) ? x : 0),
        ((y >= 0) ? y : 0)
    );
    cb();
}
