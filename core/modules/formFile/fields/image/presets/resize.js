var gm = require('gm').subClass({imageMagick: true});

var ss = module.exports = function(info) {
    this.width = info.width || null;
    this.height = info.height || null;
}

var proto = ss.prototype;
proto.pre = function(cb) {
    cb();
}

proto.make = function(info, cb) {
    info.img.resize(this.width, this.height);
    cb();
}
