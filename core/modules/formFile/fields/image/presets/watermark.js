var lib = require('./../../../lib');
var path = require('path');
var fs = require('fs.extra');
var async = require('async');
var util = require('util');
var gm = require('gm').subClass({imageMagick: true});

var wmk = module.exports = function(info) {
    this.path = 'files';
    this.position = info.position || 'center';
    this.size = {};
    this.path = path.join(lib.systemPath, this.path, (info.path || null));
}

var proto = wmk.prototype;
proto.pre = function(cb) {
    var self = this;
    if (typeof self.positions[self.position] !== 'function')
        return cb(new Error(util.format('Uncorrect format postion - %s', self.position)));

    async.waterfall([
        function(cb) {
            fs.exists(self.path, function(exist) {
                if (!exist)
                    return cb(new Error(util.format('Not find mask on path - %s', self.path)));

                gm(self.path).size(cb);
            });
        },
        function(size, cb) {
            self.size = size;
            cb();
        }
    ], cb);
}
proto.make = function(info, cb) {
    var pos = this.getPosition(info.size);
    info.img.composite(this.path).geometry(util.format('+%s+%s', pos.x, pos.y));
    cb();
}
proto.getPosition = function(imgSize) {
    return this.positions[this.position].apply(this, [imgSize]);
}

proto.positions = {
    center: function(imgSize) {
        return {
            x: (imgSize.width / 2) - (this.size.width / 2),
            y: (imgSize.height / 2) - (this.size.height / 2)
        }
    }
}
