var gm = require('gm');

var ss = module.exports = function(info) {
    this.width = info.width || 0;
    this.height = info.height || 0;
}

var proto = ss.prototype;
proto.pre = function(cb) {
    cb();
}

proto.make = function(info, cb) {
    if (info.size.height >= info.size.width)
        info.img.resize(false, this.height);
    else
        info.img.resize(this.width);

    cb();
}
