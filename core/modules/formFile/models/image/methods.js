module.exports = function(methods) {
        methods.url = function(preset) {
            if (!preset)
                return this.get('publicPath');

            preset = String(preset);
            var presets = this.get('presets');
            if (presets[preset])
                return presets[preset].public;

            return '';
        }
}