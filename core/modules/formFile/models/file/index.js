var Schema = require('mongoose').Schema;
var fields = {
    name: require('./fields/name'),
    publicPath: require('./fields/publicPath'),
    path: require('./fields/path'),
    weight: require('./fields/weight'),
    size: require('./fields/size'),
    type: require('./fields/type'),
    uploaded: require('./fields/uploaded')
}

module.exports = function() {
    var schema = new Schema(fields);
    require('./methods')(schema.methods);
    return schema;
}