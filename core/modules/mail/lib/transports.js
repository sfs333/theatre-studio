var transports = module.exports = {};
var nodemailer = require('nodemailer');

transports.smtpPool = function(opt) {
    return nodemailer.createTransport(require('nodemailer-smtp-pool')(opt));
}

transports.gmail = function(opt) {
    return nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: opt.user,
            pass: opt.pass
        }
    });
}