var moduleManager = require('moduleManager');
var async = require('async');
var util = require('util');
var log = moduleManager.use('log')(module);
var formType = moduleManager.use('formType');
var mongoose = require('mongoose');

var Build = function(model, type, callback) {
    if (typeof model == 'string')
        model = mongoose.model(model);
    var mf = this;
    type.prototype = formType;
    mf.formType = new type();
    mf.model = model;
    mf.document = false;
    mf.form = false;
    mf.saveChanges = true;

    mf.formType.on('build', function (params, callback) {
        mf.form = this;
        mf.form.mf = mf;
        callback();
    });

    mf.formType.build = function (params, callback) {
        mf = this.mf;
        mf.getDocument(params, function(err, doc) {
            if (err || !doc) return callback(err);

            mf.document = doc;
            mf.setDefaults();
            mf.setListeners();
            callback();
        });
    }

    moduleManager.use('form').registerForm(mf.formType.name, mf.formType);
    callback(null, this);
}

var proto = Build.prototype;
proto.setDefaults = function() {
    if (!this.document)
        return callback(new Error('not find document in model for set defaults'));

    var v;
    for (var path in this.form.fields) {
        if (v = this.document.get(path))
            this.form.fields[path].value = v;
    }
}

proto.save = function(cb) {
    var doc =  this.mf.document;
    var field;
    if (!doc)
        return cb(new Error('Not find document for saving form'));

    for (var path in this.fields) {
        field = this.fields[path];
        this.mf[field.multiple ? 'writeMultipleField' : 'writeField'](path, field);
    }
    if (!doc.isModified())
        return cb(null, doc);

    if (doc.isNew)
        doc = new this.mf.model(doc.toObject()); //Rebuilding for update sub documents

    doc.save(cb);
}

proto.writeField = function(path, field) {
    var doc = this.document;
    if ((typeof field.input !== 'undefined') && ((field.value !== field.input) || doc.isNew)) {
        doc.set(path, field.input);
    }
}

proto.writeMultipleField = function(path, field) {
    var doc = this.document;
    var place = doc.get(path);
    var item;
    var key;
    if (util.isArray(field.input.add)) {
        field.input.add.forEach(function(info) {
            place.push(info);
        });
    }
    if (util.isArray(field.input.edit)) {
        field.input.edit.forEach(function(info) {
            item = place.id(info.id);
            if (item) {
                delete info.id;
                for (key in info)
                    item.set(key, info[key]);
            }
        });
    }
    if (util.isArray(field.input.delete)) {
        field.input.delete.forEach(function(info) {
            item = place.id(info.id);
            if (item) item.remove();
        });
    }
}

proto.postSave = function(cb) {
    this.emit('postSave', cb);
}

proto.setValidator = function(formField) {
    if (typeof formField.on !== 'function')
    return;

    formField.on('validate', function(field, callback) {
        var doc = field.form.mf.document;
        var modelField = doc.schema.path(field.name);
        if (!modelField)
            return callback();

        modelField.doValidate(field.get(), callback, doc);
    });
}

proto.setValidators = function() {
    for (var path in this.form.fields)
        this.setValidator(this.form.fields[path]);
}

proto.setListeners = function() {
    if (this.saveChanges) {
        this.form.on('submit', this.save);
        this.form.on('submit', this.postSave);
    }
    this.form.on('submit', function(callback) {
        this.response.actions.messageWrap = {
            selector: util.format('#%s .messages', this.id),
            text: 'The changes have been saved successfully!'
        };

        callback();
    });
    this.setValidators();
}

proto.getDocument = function(params, callback) {
    if (params.document && (params.document instanceof mongoose.Document))
        return callback(null, params.document);

    if (params.id && mongoose.isObjectId(params.id))
        return this.model.findById(params.id, callback);

    return callback(null, new this.model({}));
}

module.exports.build = Build;