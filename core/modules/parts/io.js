var lib = require('./lib');

module.exports = function(io) {
    io.on('connection', function (socket) {
        socket.on('parts', function (data, cb) {
            data.context = data.context || {};
            data.context.socket = socket;
            lib.render(data.id, data.context, function(err, response) {
                    if (err)
                        return socket.request.error(err, cb);

                    cb(null, response);
            });
        });
    });
}