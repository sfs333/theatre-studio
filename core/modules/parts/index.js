var moduleManager = require('moduleManager');
var publicDepends = moduleManager.use('publicDepends');
module.exports = require('./lib');

module.exports.run = function(app, cb) {
    require('./io')(app.get('io'));
    cb();
}

publicDepends.addDependency({parts: {scripts: ['js/parts/index'], static: true}});