var modules = require('modules');

var popupHandlers = {};
module.exports.run = function(app, cb) {
    require('./popups');
    cb();
}
module.exports.popupHandlers = popupHandlers;
modules.use('publicDepends').addDependency({
    popup: {
        scripts: ['js/popup/index', 'js/popup/popup-form', 'js/popup/popup-img'],
        dependencies: ['jqUiDialog'],
        static: true
    }
});
module.exports.add = function(id, info) {
    return modules.use('parts').add('popup_' + id, info);
}