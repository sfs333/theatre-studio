var moduleManager = require('moduleManager');
var winston = require('winston');
var path = require('path');
var config = moduleManager.use('config');

function getLogger(module) {
    var dev = (config.get('env') == 'development');
    var ModulePath = module.filename.split('/').slice(-2).join('/');
    var level = dev ? 'debug' : 'error';
    var logFile = config.get('logFile');
    var transports = [
        new winston.transports.Console({
            colorize: true,
            level: level,
            label: ModulePath
        })
    ];
    if (logFile) {
        transports.push(
            new (winston.transports.File)({
                filename: path.normalize( __dirname + '/../../' + logFile),
                level: level,
                label: ModulePath
            })
        );
    }
    var logger = new winston.Logger({transports: transports});
    var defaultErrLogger = logger.error;
    var defaultWarnLogger = logger.warn;
    logger.error = function(err) {
        defaultErrLogger((typeof err.message === 'string') ? err.message : err);
        if (dev) console.trace();
    }
    logger.warn = function(err) {
        defaultWarnLogger((typeof err.message === 'string') ? err.message : err);
    }

    return logger;
}

module.exports = getLogger;